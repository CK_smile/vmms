from django.utils.encoding import iri_to_uri
from django.shortcuts import render,redirect
from django.core.urlresolvers import reverse
from django.conf import settings
from django.http import HttpResponseRedirect
from django.core.cache import cache

class ClearCacheMiddleware(object):
    def process_request(self, request):
        refresh_field_name = 'clearcache'
        is_refresh = request.POST.get(refresh_field_name,
                                      request.GET.get(refresh_field_name, 'False'))
        if is_refresh == 'True':
            cache.clear()
            return redirect(request.path)
