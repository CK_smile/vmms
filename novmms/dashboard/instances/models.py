from django.utils import timezone
from django.db import models
from model_utils.models import TimeStampedModel
from novmms import api

import logging
logger = logging.getLogger(__name__)

# use Instance.save() to selfupdate the modified field
class Instance(TimeStampedModel):
    """
    uuid: the instance's uuid in Openstack
    addresses: the instance's ip address
    name: the instance's name
    host: on which host the instance locates
    availability_zone: availability_zone
    launched_at: datetime when the instance launched
    flavor: the server type
    image: the image used to launch instance
    project: the project(tenant) instance belong to
    owner: the owner of the instance(map to ldap users)
    """
    uuid = models.CharField(max_length=255,primary_key=True)
    addresses = models.CharField(max_length=255, null=True, blank=True)
    name = models.CharField(max_length=255, null=True)
    key_name = models.CharField(max_length=255, null=True, blank=True)
    status = models.CharField(max_length=255, null=True, blank=True)
    power_state = models.CharField(max_length=255, null=True, blank=True)
    host = models.CharField(max_length=255, null=True)
    availability_zone = models.CharField(max_length=255, null=True)
    launched_at = models.DateTimeField(null=True)
    flavor = models.CharField(max_length=255, null=True)
    image = models.CharField(max_length=255, null=True)
    project = models.CharField(max_length=255, null=True)
    project_name = models.CharField(max_length=255, null=True)
    owner = models.CharField(max_length=255, null=True)
    def __unicode__(self):
        return u'%s' % (self.name,)
    def save(self, *args, **kwargs):
        logger.debug('Instance %s created or updated' % self.uuid)
        self.project_name = api.keystone.tenant_list().get(self.project, None)
        super(Instance, self).save(*args, **kwargs)
    class Meta:
        db_table = "instances"

