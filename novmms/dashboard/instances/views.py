#coding=utf-8
import json
from collections import OrderedDict

from django.conf import settings
from django.shortcuts import render,redirect
from django.utils import timezone
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.views import generic
from django.contrib.auth.decorators import login_required
from django.db.utils import IntegrityError
from django.core.cache import cache
from django.contrib.auth.decorators import permission_required
from django.template.defaultfilters import slugify

from novmms import api
from novmms.dashboard import utils
from novmms.dashboard.instances.models import Instance
from novmms.dashboard.instances.forms import LaunchInstanceForm, SetIPForm

import logging
logger = logging.getLogger(__name__)

PERMISSION_REQUIRED = settings.PERMISSION_REQUIRED

INFO_TEMPLATE = 'novmms/dashboard/instances/info.html'

@login_required
@permission_required(PERMISSION_REQUIRED['user'])
def index(request):
    """
    List the current users' instances
    """
    errmsg = ''
    logger.debug('User %s' % request.user.username)
    try:
        launchForm, setIPForm = (LaunchInstanceForm(request), SetIPForm(request)) if request.user.has_perm('openstack.roles.leader') else (None, None)
        refresh_field_name = 'refresh'
        is_refresh = request.POST.get(refresh_field_name,
                                      request.GET.get(refresh_field_name, 'False'))
        instances = []
        if is_refresh == 'True':
            # read from nova-api
            if request.user.has_perm('openstack.roles.admin'):
                instances = api.nova.server_list(request, all_tenants = True)
            elif request.user.has_perm('openstack.roles.leader'):
                instances = api.nova.server_list(request)
            else:
                myInstances = Instance.objects.filter(owner=request.user.username)
                for inst in myInstances:
                    inst_info = api.nova.server_get(request, inst.uuid)
                    inst_info.owner = inst.owner
                    instances.append(inst_info)
            utils.handle_instances(request, instances)
        else:
            # read from db
            if request.user.has_perm('openstack.roles.admin'):
                instances = Instance.objects.all()
            elif request.user.has_perm('openstack.roles.leader'):
                instances = Instance.objects.filter(project=request.user.tenant_id)
            else:
                instances = Instance.objects.filter(owner=request.user.username)
    except Exception as exc:
        logger.error('Exception happens: %s' % str(exc))
        errmsg = str(exc)
    return render(request, 'novmms/dashboard/instances/index.html',
                  {'setIPForm': setIPForm, 'launchForm': launchForm,
                   'instances': instances, 'errmsg': errmsg, })

@login_required
@permission_required(PERMISSION_REQUIRED['user'])
def console(request, instance_id):
    """
    Display the instance's console
    """
    logger.debug('User %s, Instance %s' % (request.user.username, instance_id))
    info_label = '虚拟机console'
    msg = 'console OK'
    try:
#        instance = api.nova.server_get(request, instance_id)
        console_url = api.nova.server_vnc_console(request, instance_id)
        return redirect(console_url)
    except Exception as exc:
        msg = str(exc)
        return render(request, INFO_TEMPLATE, {'label': info_label, 'info': msg, })

@login_required
@permission_required(PERMISSION_REQUIRED['user'])
def detail(request):
    """
    Show the detail of the instance
    """
    pass

ACTIVE_STATES = ("ACTIVE",)
def is_deleting(instance):
    task_state = getattr(instance, "OS-EXT-STS:task_state", None)
    if not task_state:
        return False
    return task_state.lower() == "deleting"
@login_required
@permission_required(PERMISSION_REQUIRED['user'])
def reboot(request, instance_id):
    """
    Hard reboot the instance
    """
    logger.debug('User %s, Instance %s' % (request.user.username, instance_id))
    info_label = '硬重启虚拟机'
    msg = 'Hard Reboot OK'
    try:
        instance = api.nova.server_get(request, instance_id)
        if instance is not None:
            if ((instance.status in ACTIVE_STATES
                 or instance.status == 'SHUTOFF')
                and not is_deleting(instance)):
                api.nova.server_reboot(request, instance_id, soft_reboot=False)
    except Exception as exc:
        msg = str(exc)
    return render(request, INFO_TEMPLATE, {'label': info_label, 'info': msg, })

@login_required
@permission_required(PERMISSION_REQUIRED['user'])
def soft_reboot(request, instance_id):
    """
    Soft reboot the instance
    """
    logger.debug('User %s, Instance %s' % (request.user.username, instance_id))
    info_label = '软重启虚拟机'
    msg = 'Soft Reboot OK'
    try:
        api.nova.server_reboot(request, instance_id, soft_reboot=True)
    except Exception as exc:
        msg = str(exc)
    return render(request, INFO_TEMPLATE, {'label': info_label, 'info': msg, })

@login_required
@permission_required(PERMISSION_REQUIRED['user'])
def shutoff(request, instance_id):
    """
    Shut Off the instance
    """
    logger.debug('User %s, Instance %s' % (request.user.username, instance_id))
    info_label = '关闭虚拟机'
    msg = 'ShutOff OK'
    try:
        instance = api.nova.server_get(request, instance_id)
        if instance is not None:
            if ((api.utils.get_power_state(instance) in ("running", "suspended"))
                and not is_deleting(instance)):
                api.nova.server_stop(request, instance_id)
    except Exception as exc:
        msg = str(exc)
    return render(request, INFO_TEMPLATE, {'label': info_label, 'info': msg, })

@login_required
@permission_required(PERMISSION_REQUIRED['user'])
def start(request, instance_id):
    """
    Start the instance
    """
    logger.debug('User %s, Instance %s' % (request.user.username, instance_id))
    info_label = '启动虚拟机'
    msg = 'start OK'
    try:
        instance = api.nova.server_get(request, instance_id)
        if instance is not None:
            if instance.status in ("SHUTDOWN", "SHUTOFF", "CRASHED"):
                api.nova.server_start(request, instance_id)
    except Exception as exc:
        msg = str(exc)
    return render(request, INFO_TEMPLATE, {'label': info_label, 'info': msg, })

@login_required
@permission_required(PERMISSION_REQUIRED['admin'])
def launch(request):
    """
    launch new instance
    """
    logger.debug('User %s' % request.user.username)
    info_label = '创建虚拟机'
    msg = 'launch instance OK'
    try:
        form = LaunchInstanceForm(request, request.POST)
        if form.is_valid():
            msg = 'ok'
            data = form.cleaned_data
            count = data['count'] if data['count'] else 1
            nics = []
            hints = None
            for p in data['port_id'].split(','):
                nics.append({'port-id': p})
            if data['host'] and data['same_host']:
                msg = 'host and same_host cant be set at same time'
            if data['host']:
                host_list = api.nova.zone_host_map(request).get(data['availzones'], [])
                if data['host'] not in host_list:
                    msg = 'Failed:host not in AZ'
                else:
                    data['availzones'] += ':' + data['host']
            if data['same_host']:
                hints = {'same-host': data['same_host']}
            if msg == 'ok':
                # call nova to create server
                instance = api.nova.server_create(request,
                                                  data['name'],
                                                  data['image'],
                                                  data['flavor'],
                                                  data['keypair_name'],
                                                  None,
                                                  ['default'],
                                                  block_device_mapping=None,
                                                  block_device_mapping_v2=None,
                                                  nics=nics,
                                                  availability_zone=data['availzones'],
                                                  scheduler_hints=hints,
                                                  instance_count=count,
                                                  admin_pass=None,
                                                  disk_config=None,
                                                  config_drive=None)
                # create this Instance in db
                Instance.objects.create(uuid=instance.id,
                                    addresses=api.utils.format_addresses(instance.addresses),
                                    name=data['name'],
                                    key_name=data['keypair_name'],
                                    status=instance.status,
                                    power_state=api.utils.get_power_state(instance),
                                    host=data['host'],# api.nova.get_host(instance.id),
                                    availability_zone=data['availzones'],
                                    launched_at=instance.created,
                                    flavor=api.nova.flavor_list(request)[data['flavor']]['name'],
                                    image=api.glance.image_list_detailed(request)[data['image']]['name'],
                                    project=instance.tenant_id,
                                    owner=data['owner'])
    except Exception as exc:
        msg = str(exc)
    return render(request, INFO_TEMPLATE, {'label': info_label, 'info': msg, })

@login_required
@permission_required(PERMISSION_REQUIRED['admin'])
def delete(request, instance_id):
    """
    delete the instance
    """
    logger.debug('User %s, Instance %s' % (request.user.username, instance_id))
    info_label = '删除虚拟机'
    msg = 'delete OK'
    try:
        api.nova.server_delete(request, instance_id)
        # delete in db
        Instance.objects.get(uuid=instance_id).delete()
    except Exception as exc:
        msg = str(exc)
    return render(request, INFO_TEMPLATE, {'label': info_label, 'info': msg, })

@login_required
@permission_required(PERMISSION_REQUIRED['admin'])
def setip(request):
    """
    generate an IP addr
    """
    logger.debug('User %s' % request.user.username)
    info_label = '生成的ip是'
    port = 'ip allocate failed'
    try:
        form = SetIPForm(request, request.POST)
        if form.is_valid():
            count = form.cleaned_data['ip_count']
            count = count if count else 1
            ip_addr = form.cleaned_data['ip_addr']
            availzone = form.cleaned_data['availzones']
            network = api.neutron.network_list_for_tenant(request,
                                                          request.user.tenant_id).get(form.cleaned_data['networks'], None)
            if network:
                # 查找选择的网络中包含此可用域对应的subnet
                subnets = dict([(sub['name'], sub) for sub in network['subnets']])
                for netname in getattr(settings, 'AVAILZONES_SUBNET_MAPPING', {})[availzone]:
                    if netname in subnets: # 此时，进行ip分配
                        subnetinfo = subnets[netname]
                        if ip_addr != '':
                            port = api.neutron.port_create(request, network_id=network['id'],
                                                           subnet_id=subnetinfo['id'],
                                                           ip_addr=ip_addr)
                        else:
                            port = []
                            for i in range(count):
                                port.append(api.neutron.port_create(request, network_id=network['id'],
                                                                    subnet_id=subnetinfo['id']))
    except Exception as exc:
        port = str(exc)
    return render(request, INFO_TEMPLATE, {'label': info_label, 'info': json.dumps(port, indent=4), })

@login_required
@permission_required(PERMISSION_REQUIRED['admin'])
#@permission_required(PERMISSION_REQUIRED['superadmin'])
def coldmigrate(request, instance_id):
    logger.debug('User %s, Instance %s' % (request.user.username, instance_id))
    info_label = '冷迁移'
    msg = 'cold migrate OK'
    try:
        host = request.POST.get('host')
        if host:
            api.nova.server_migrate(request, instance_id, host)
            api.nova.server_confirm_resize(request, instance_id)
    except Exception as exc:
        msg = str(exc)
    return render(request, INFO_TEMPLATE, {'label': info_label, 'info': msg, })

@login_required
@permission_required(PERMISSION_REQUIRED['admin'])
#@permission_required(PERMISSION_REQUIRED['superadmin'])
def coldmigrate_confirm(request, instance_id):
    logger.debug('User %s, Instance %s' % (request.user.username, instance_id))
    info_label = '冷迁移确认'
    msg = 'confirm OK'
    try:
        if instance_id:
            api.nova.server_confirm_resize(request, instance_id)
    except Exception as exc:
        msg = str(exc)
    return render(request, INFO_TEMPLATE, {'label': info_label, 'info': msg, })

@login_required
@permission_required(PERMISSION_REQUIRED['admin'])
def attach_interface(request, instance_id):
    logger.debug('User %s, Instance %s' % (request.user.username, instance_id))
    info_label = '增加网络'
    msg = 'attach_interface OK'
    try:
        port_id = request.POST.get('port_id', None)
        if instance_id and port_id:
            api.nova.interface_attach(request, instance_id, port_id)
    except Exception as exc:
        msg = str(exc)
    return render(request, INFO_TEMPLATE, {'label': info_label, 'info': msg, })

@login_required
@permission_required(PERMISSION_REQUIRED['admin'])
def detach_interface(request, instance_id):
    logger.debug('User %s, Instance %s' % (request.user.username, instance_id))
    info_label = '删除网络'
    msg = 'detach_interface Failed'
    try:
        port_id = request.POST.get('port_id', None)
        if instance_id and port_id:
            api.nova.interface_detach(request, instance_id, port_id)
    except Exception as exc:
        msg = str(exc)
    return render(request, INFO_TEMPLATE, {'label': info_label, 'info': msg, })

@login_required
@permission_required(PERMISSION_REQUIRED['admin'])
def setkey(request):
    """
    delete the instance
    """
    logger.debug('User %s' % request.user.username)
    info_label = '生成的private key是'
    msg = 'setkey OK'
    try:
        name = request.POST.get('keypair_name', None)
        if name:
            keypair = api.nova.keypair_create(request, name)
            msg = keypair.private_key
            response = HttpResponse(content_type='application/binary')
            response['Content-Disposition'] = ('attachment; filename=%s.pem'
                                               % slugify(name))
            response.write(keypair.private_key)
            response['Content-Length'] = str(len(response.content))
            return response
    except Exception as exc:
        msg = str(exc)
    return render(request, INFO_TEMPLATE, {'label': info_label, 'info': msg, })

