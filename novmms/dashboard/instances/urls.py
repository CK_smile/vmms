from django.conf.urls import url

from novmms.dashboard.instances import views

INSTANCES = r'^(?P<instance_id>[^/]+)/%s$'

urlpatterns = [
    url(r'^/*$', views.index, name='index'),
    url(INSTANCES % 'console', views.console, name='console'),
    url(INSTANCES % 'reboot', views.reboot, name='reboot'),
    url(INSTANCES % 'soft_reboot', views.soft_reboot, name='soft_reboot'),
    url(INSTANCES % 'shutoff', views.shutoff, name='shutoff'),
    url(INSTANCES % 'start', views.start, name='start'),
    url(INSTANCES % 'attach_interface', views.attach_interface,
        name='attach_interface'),
    url(INSTANCES % 'detach_interface', views.detach_interface,
        name='detach_interface'),
    url(INSTANCES % 'migrate', views.coldmigrate, name='migrate'),
    url(INSTANCES % 'migrate_confirm', views.coldmigrate_confirm,
        name='migrate_confirm'),
    url(r'^launch$', views.launch, name='launch'),
    url(r'^setip$', views.setip, name='setip'),
    url(r'^setkey$', views.setkey, name='setkey'),
    url(INSTANCES % 'delete', views.delete, name='delete'),
    url(INSTANCES % 'detail', views.detail, name='detail'),
]
