#coding=utf-8
from django import forms
from django.core.cache import cache
from novmms import api

class LaunchInstanceForm(forms.Form):
    name = forms.CharField(
        widget=forms.TextInput(),
        max_length=255,label=u"名称")
    availzones = forms.ChoiceField(label=u"可用域", required=True)
    host = forms.CharField(
        widget=forms.TextInput(),
        max_length=255,label=u"宿主机",required=False)
    same_host = forms.CharField(
        widget=forms.TextInput(),
        max_length=255,label=u"与哪台虚机一块",required=False)
    keypair_name = forms.CharField(
        widget=forms.TextInput(),
        max_length=1000,label=u"密钥名称")
    port_id = forms.CharField(
        widget=forms.TextInput(),
        max_length=1000,label=u"ip地址")
    image = forms.ChoiceField(label=u"镜像名", required=True)
    flavor = forms.ChoiceField(label=u"主机类型", required=True)
    count = forms.IntegerField(label=u"数量", min_value=1, max_value=5,
                               required=False)
    owner = forms.CharField(
        widget=forms.TextInput(),
        max_length=255,label=u"拥有者")

    def __init__(self, request, *args, **kwargs):
        super(LaunchInstanceForm, self).__init__(*args, **kwargs)
        self.fields['image'].choices = [(iid, i['name']) for iid, i in
                                       api.glance.image_list_detailed(request).items()]
        self.fields['flavor'].choices = [(fid, f['name']) for fid, f in
                                        api.nova.flavor_list(request).items()]
        self.fields['availzones'].choices = [(az, az) for az in
                                             api.nova.availability_zone_list(request)]

class SetIPForm(forms.Form):
    availzones = forms.ChoiceField(label=u"可用域", required=True)
    networks = forms.ChoiceField(label=u"网络", required=True)
    ip_addr = forms.CharField(
        widget=forms.TextInput(),
        max_length=255,label=u"指定IP",required=False)
    ip_count = forms.IntegerField(label=u"连续分配n个", min_value=1, max_value=5,
                               required=False)

    def __init__(self, request, *args, **kwargs):
        super(SetIPForm, self).__init__(*args, **kwargs)
        self.fields['networks'].choices = self.get_networks(request)
        self.fields['availzones'].choices = [(az, az) for az in
                                             api.nova.availability_zone_list(request)]

    @staticmethod
    # {id: <class Network>, ...}
    def get_networks(request):
        tenant_id = request.user.tenant_id
        networks = api.neutron.network_list_for_tenant(
            request, tenant_id, include_external=True)
        return [(i, n['name']) for i, n in networks.items()]

