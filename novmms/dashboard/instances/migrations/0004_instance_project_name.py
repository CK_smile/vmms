# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instances', '0003_auto_20161008_0728'),
    ]

    operations = [
        migrations.AddField(
            model_name='instance',
            name='project_name',
            field=models.CharField(max_length=255, null=True),
        ),
    ]
