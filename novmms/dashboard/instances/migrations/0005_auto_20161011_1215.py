# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instances', '0004_instance_project_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='instance',
            name='addresses',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='instance',
            name='key_name',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='instance',
            name='power_state',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='instance',
            name='status',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
