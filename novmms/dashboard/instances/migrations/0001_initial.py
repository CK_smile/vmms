# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Instance',
            fields=[
                ('uuid', models.CharField(max_length=255, serialize=False, primary_key=True)),
                ('addresses', models.CharField(max_length=255, null=True)),
                ('name', models.CharField(max_length=255, null=True)),
                ('host', models.CharField(max_length=255, null=True)),
                ('availability_zone', models.CharField(max_length=255, null=True)),
                ('launched_at', models.DateTimeField(null=True)),
                ('flavor', models.CharField(max_length=255, null=True)),
                ('image', models.CharField(max_length=255, null=True)),
                ('project', models.CharField(max_length=255, null=True)),
                ('owner', models.CharField(max_length=192, null=True)),
            ],
            options={
                'db_table': 'instances',
            },
        ),
    ]
