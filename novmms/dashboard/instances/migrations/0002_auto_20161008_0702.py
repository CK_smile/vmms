# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instances', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='instance',
            name='key_name',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='instance',
            name='power_state',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='instance',
            name='status',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='instance',
            name='owner',
            field=models.CharField(max_length=255, null=True),
        ),
    ]
