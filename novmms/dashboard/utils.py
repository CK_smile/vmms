from novmms.dashboard.instances.models import Instance
from novmms import api

# request can be None
def handle_instances(request, instances, is_admin=False):
    if instances:
        full_flavors = api.nova.flavor_list(request, is_admin=is_admin)
        image_map = api.glance.image_list_detailed(request, is_admin=is_admin)
        # Loop through instances to get flavor info.
        for instance in instances:
            if hasattr(instance, 'image'):
                # Instance from image returns dict
                if isinstance(instance.image, dict):
                    if instance.image.get('id') in image_map:
                        instance.image = image_map[instance.image['id']]['name']
            flavor_id = instance.flavor["id"]
            if flavor_id in full_flavors:
                instance.flavor = full_flavors[flavor_id]['name']
            else:
                # If the flavor_id is not in full_flavors list,
                # get it via nova api.
                the_flavor = api.nova.flavor_get(request, flavor_id,
                                                 is_admin=is_admin)
                instance.flavor = the_flavor['name']
            instance.addresses = api.utils.format_addresses(instance.addresses)
            instance.power_state = api.utils.get_power_state(instance)
            instance.uuid = instance.id
            dataObj = Instance.objects.get(uuid=instance.id)
            dataObj.addresses = instance.addresses
            dataObj.status = instance.status
            dataObj.power_state = instance.power_state
            dataObj.save()
            instance.owner = dataObj.owner
            instance.modified = dataObj.modified
            instance.availability_zone = dataObj.availability_zone

