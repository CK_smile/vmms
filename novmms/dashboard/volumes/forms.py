#coding:utf-8
from django import forms
from django.core.cache import cache
from novmms import api
from novmms.dashboard.instances.forms import SetIPForm

class CreateVolumeForm(forms.Form):
    name = forms.CharField(
        widget=forms.TextInput(),
        max_length=255,label=u"名称", required=True)
    availzones = forms.ChoiceField(label=u"可用域", required=True)
    snapshot_source = forms.CharField(label=u"从snapshot建立卷",required=False)
    local_to_instance = forms.CharField(label=u"与哪台虚机一块",required=False)
    size = forms.IntegerField(label=u"大小(G)", min_value=1,
                              required=True)

    def __init__(self, request, *args, **kwargs):
        super(CreateVolumeForm, self).__init__(*args, **kwargs)
        self.fields['availzones'].choices = [(az, az) for az in
                                             api.nova.availability_zone_list(request)]
