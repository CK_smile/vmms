#coding=utf-8
from django.utils import functional
from django.utils import timezone
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic
from django.contrib.auth.decorators import login_required, permission_required
from django.conf import settings

from novmms import api
from novmms.dashboard.volumes.forms import CreateVolumeForm

import logging
logger = logging.getLogger(__name__)

PERMISSION_REQUIRED = settings.PERMISSION_REQUIRED

INFO_TEMPLATE = 'novmms/dashboard/instances/info.html'

@login_required
@permission_required(PERMISSION_REQUIRED['user'])
def index(request):
    """
    List the current users' volumes
    """
    logger.debug('User %s' % request.user.username)
    errmsg = ''
    try:
        createVolForm = CreateVolumeForm(request) if request.user.has_perm('openstack.roles.leader') else None
        volumes = api.cinder.volume_list(request)
        for v in volumes:
            for att in v.attachments:
                if att.get('server_id', None):
                    server = api.nova.server_get(request, att['server_id'])
                    att.update({'server_name': server.name})
    except Exception as exc:
        errmsg = str(exc)
    return render(request, 'novmms/dashboard/volumes/index.html',
                  {'createVolForm': createVolForm, 'volumes': volumes,
                   'errmsg': errmsg})

@login_required
@permission_required(PERMISSION_REQUIRED['admin'])
def create(request):
    """
    create the new volume
    """
    logger.debug('User %s' % request.user.username)
    info_label = '创建存储卷'
    msg = 'create volume OK'
    try:
        form = CreateVolumeForm(request, request.POST)
        if form.is_valid():
            msg = 'ok'
            data = form.cleaned_data
            hints = None
            # availzone:hostname can't be used
    #        if data['host'] and data['local_to_instance']:
    #            msg = 'host and same_host cant be set at same time'
    #        if data['host']:
    #            host_list = api.nova.zone_host_map(request).get(data['availzones'], [])
    #            if data['host'] not in host_list:
    #                msg = 'Failed:host not in AZ'
    #            else:
    #                data['availzones'] += ':' + data['host']
            if data['local_to_instance']:
                hints = {'local_to_instance': data['local_to_instance']}
            if data.get("snapshot_source", None):
                # Create from Snapshot
                snapshot = api.cinder.volume_snapshot_get(request,
                                                          data["snapshot_source"])
                snapshot_id = snapshot.id
                if (data['size'] < snapshot.size):
                    msg = ('The volume size cannot be less than the snapshot size (%sGiB)' % snapshot.size)
                # az = None
            if msg == 'ok':
                volume = api.cinder.volume_create(request,
                                                  data['size'],
                                                  data['name'],
                                                  '', # desc
                                                  '', # type
                                                  availability_zone=data['availzones'],
                                                  snapshot_id=data.get("snapshot_source",
                                                                       None),
                                                  scheduler_hints=hints)
    except Exception as exc:
        msg = str(exc)
    return render(request, INFO_TEMPLATE, {'label': info_label, 'info': msg, })

@login_required
@permission_required(PERMISSION_REQUIRED['admin'])
def delete(request, volume_id):
    """
    Show the detail of the instance
    """
    logger.debug('User %s' % request.user.username)
    info_label = '删除存储卷'
    msg = 'delete OK'
    try:
        api.cinder.volume_delete(request, volume_id)
    except Exception as exc:
        msg = str(exc)
    return render(request, INFO_TEMPLATE, {'label': info_label, 'info': msg, })

@login_required
@permission_required(PERMISSION_REQUIRED['admin'])
def attach(request, volume_id):
    """
    Show the detail of the instance
    """
    logger.debug('User %s, Volume_id %s' % (request.user.username, volume_id))
    info_label = '挂载存储卷'
    msg = 'attach OK'
    try:
        attach_to = request.POST.get('attach_to', None)
        if attach_to and volume_id:
            api.nova.instance_volume_attach(request, volume_id, attach_to)
    except Exception as exc:
        msg = str(exc)
    return render(request, INFO_TEMPLATE, {'label': info_label, 'info': msg, })

@login_required
@permission_required(PERMISSION_REQUIRED['admin'])
def detach(request, volume_id):
    """
    Show the detail of the instance
    """
    logger.debug('User %s, Volume_id %s' % (request.user.username, volume_id))
    info_label = '卸载存储卷'
    msg = 'detach OK'
    try:
        instance_id = request.POST.get('instance_id', None)
        if instance_id and volume_id:
            api.nova.instance_volume_detach(request, instance_id, volume_id)
    except Exception as exc:
        msg = str(exc)
    return render(request, INFO_TEMPLATE, {'label': info_label, 'info': msg, })

@login_required
@permission_required(PERMISSION_REQUIRED['admin'])
def create_snapshot(request, volume_id):
    """
    """
    logger.debug('User %s, Volume_id %s' % (request.user.username, volume_id))
    info_label = '创建快照'
    msg = 'create snapshot OK'
    try:
        name = request.POST.get('name', None)
        desc = request.POST.get('desc', '')
        if name and volume_id:
            force = False
            volume = api.cinder.volume_get(request,
                                           volume_id)
            if volume.status == 'in-use':
                force = True
            api.cinder.volume_snapshot_create(request,
                                              volume_id,
                                              name,
                                              desc,
                                              force=force)
    except Exception as exc:
        msg = str(exc)
    return render(request, INFO_TEMPLATE, {'label': info_label, 'info': msg, })

@login_required
@permission_required(PERMISSION_REQUIRED['admin'])
def migrate(request, volume_id):
    """
    """
    logger.debug('User %s, Volume_id %s' % (request.user.username, volume_id))
    info_label = '迁移存储卷'
    msg = 'migrate OK'
    try:
        migrate_to = request.POST.get('migrate_to', None)
        if migrate_to:
            api.cinder.volume_migrate(request, volume_id, migrate_to)
    except Exception as exc:
        msg = str(exc)
    return render(request, INFO_TEMPLATE, {'label': info_label, 'info': msg, })

@login_required
@permission_required(PERMISSION_REQUIRED['user'])
def detail(request, volume_id):
    """
    """
    pass

