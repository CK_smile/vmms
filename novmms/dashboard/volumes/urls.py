from django.conf.urls import url

from novmms.dashboard.volumes import views

VOLUMES = r'^(?P<volume_id>[^/]+)/%s$'

urlpatterns = [
    url(r'^/*$', views.index, name='index'),
    url(r'^create/$', views.create, name='create'),
    url(VOLUMES % 'delete', views.delete, name='delete'),
    url(VOLUMES % 'attach', views.attach, name='attach'),
    url(VOLUMES % 'detach', views.detach, name='detach'),
    url(VOLUMES % 'create_snapshot', views.create_snapshot, name='create_snapshot'),
    url(VOLUMES % 'migrate', views.migrate, name='migrate'),
    url(VOLUMES % 'detail', views.detail, name='detail'),
]
