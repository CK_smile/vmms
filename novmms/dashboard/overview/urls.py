from django.conf.urls import url

from novmms.dashboard.overview import views

urlpatterns = [
    url(r'^/*$', views.index, name='index'),
]
