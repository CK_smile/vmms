#coding=utf-8
from django.utils import functional
from django.utils import timezone
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic
from django.contrib.auth.decorators import login_required, permission_required
from django.conf import settings

from novmms import api
from novmms.dashboard.instances.models import Instance

PERMISSION_REQUIRED = settings.PERMISSION_REQUIRED

@login_required
@permission_required(PERMISSION_REQUIRED['user'])
def index(request):
    """
    List the current users' volumes
    """
    errmsg = ''
    try:
        # count instances
        is_leader = request.user.has_perm('openstack.roles.leader')
        instances = []
        if is_leader:
            try:
                instances = api.nova.server_list(request, search_opts={})
            except Exception:
                instances = []
        else:
            myInstances = Instance.objects.filter(owner=request.user.username)
            for inst in myInstances:
                inst_info = api.nova.server_get(request, inst.uuid)
                inst_info.owner = inst.owner
                instances.append(inst_info)
        # count volumes
        volumes_count = 0
        volumes_size = 0
        if is_leader:
            volumes = api.cinder.volume_list(request)
            for v in volumes:
                volumes_count += 1
                volumes_size += v.size
        else:
            for inst in instances:
                for vid in getattr(inst, 'os-extended-volumes:volumes_attached', []):
                    vol = api.cinder.volume_get(request, vid['id'])
                    volumes_count += 1
                    volumes_size += vol.size
        # count cpus, ram
        compute_limits = api.nova.instance_usage(request)
        core_count = compute_limits.get('totalCoresUsed', 0)
        ram_count = compute_limits.get('totalRAMUsed', 0)
        # get quota
        quotas = {}
        if is_leader:
            tenant_id = request.user.tenant_id
            compute_quotas = api.nova.tenant_quota_get(request, tenant_id)
            storage_quotas = api.cinder.tenant_quota_get(request, tenant_id)
            quotas = {
                'instance_count': compute_quotas.instances,
                'volumes_count': storage_quotas.volumes,
                'volumes_size': storage_quotas.gigabytes,
                'core_count': compute_quotas.cores,
                'ram_count': compute_quotas.ram
            }
        usage_count = {
            'instance_count': len(instances),
            'volumes_count': volumes_count,
            'volumes_size': volumes_size,
            'core_count': core_count,
            'ram_count': ram_count
        }
    except Exception as exc:
        errmsg = str(exc)
    return render(request, 'novmms/dashboard/overview/index.html',
                  {'usage': usage_count, 'quotas': quotas, 'errmsg': errmsg})
