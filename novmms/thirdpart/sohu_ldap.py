#coding=utf-8
import re
import ldap

"""
Just for ldap auth
"""

class LdapAuth:
    def __init__(self):
        ldap.set_option(ldap.OPT_REFERRALS, 0)
        ldap.protocol_version = 3
        self.ldapConn = ldap.initialize('ldap://ldap.sohu-inc.com')

    def Auth(self, userName, passWord):
        try:
            if not userName.endswith("@sohu-inc.com"):
                userName = userName + "@sohu-inc.com"
            self.ldapConn.simple_bind_s(userName, passWord)
            filter = "mail=" + userName
            baseDn = "OU=SOHU-USERS, DC=sohu-inc, DC=com"
            res = self.ldapConn.search_st(baseDn, ldap.SCOPE_SUBTREE, filter)
            for row in res:
                attribDict = row[1]
                # dn='钱熙(网络运营部),OU=Tech-NO,OU=SOHUGROUP,OU=SOHU-USERS,DC=sohu-inc,DC=com'
                dn = attribDict['distinguishedName'][0].decode('utf-8')
                department = dn[dn.index('OU=')+3:dn.index(',', dn.index('OU='))]
                # tmpstr='钱熙(网络运营部)'
                tmpstr = attribDict['cn'][0].decode('utf-8')
                department_desc = tmpstr[tmpstr.index('(')+1:-1]
                retdata = {
                    'username': attribDict['mailNickname'][0],
                    'password': passWord,
                    'project': department,
                    'email': attribDict['mail'][0],
                    'project_description': department_desc,
                    'role': 'stuff',
                }
                return retdata
        except:
            return None 

if __name__ == "__main__":
    print LdapAuth().Auth("xiqian", "y+Cqgnw23er")
