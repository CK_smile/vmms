from django.conf import settings
from django.core.cache import cache
from novmms.api import base, utils
from cinderclient.v2 import client as cinder_client_v2

def cinderclient(request):
    insecure = getattr(settings, 'OPENSTACK_SSL_NO_VERIFY', False)
    cacert = getattr(settings, 'OPENSTACK_SSL_CACERT', None)
    cinder_url = utils.url_for(request, service_type = 'volumev2')
    c = cinder_client_v2.Client(request.user.username,
                                     request.user.token.id,
                                     project_id=request.user.tenant_id,
                                     auth_url=cinder_url,
                                     insecure=insecure,
                                     cacert=cacert,
                                     http_log_debug=settings.DEBUG)
    c.client.auth_token = request.user.token.id
    c.client.management_url = cinder_url
    return c

def volume_list(request, search_opts=None):
    """To see all volumes in the cloud as an admin you can pass in a special
    search option: {'all_tenants': 1}
    """
    c_client = cinderclient(request)
    volumes = []
    for v in c_client.volumes.list(search_opts=search_opts):
        # https://wiki.openstack.org/wiki/VolumeTransfer
        # v.transfer = transfers.get(v.id)
        volumes.append(v)

    return volumes

def volume_get(request, volume_id):
    return cinderclient(request).volumes.get(volume_id)

def volume_create(request, size, name, description, volume_type,
                  snapshot_id=None, metadata=None, image_id=None,
                  availability_zone=None, source_volid=None,
                  scheduler_hints=None):
    data = {'name': name,
            'description': description,
            'volume_type': volume_type,
            'snapshot_id': snapshot_id,
            'metadata': metadata,
            'imageRef': image_id,
            'availability_zone': availability_zone,
            'source_volid': source_volid,
            'scheduler_hints': scheduler_hints}

    volume = cinderclient(request).volumes.create(size, **data)
    return volume

def volume_delete(request, volume_id):
    return cinderclient(request).volumes.delete(volume_id)

def volume_migrate(request, volume_id, host, force_host_copy=False,
                   lock_volume=False):
    return cinderclient(request).volumes.migrate_volume(volume_id,
                                                        host,
                                                        force_host_copy,
                                                        lock_volume)

def volume_snapshot_get(request, snapshot_id):
    snapshot = cinderclient(request).volume_snapshots.get(snapshot_id)
    return snapshot

def volume_snapshot_create(request, volume_id, name,
                           description=None,
                           force=False):
    # force = True if volume.status=in-use
    data = {'name': name,
            'description': description,
            'force': force}
    return cinderclient(request).volume_snapshots.create(volume_id, **data)

def tenant_quota_get(request, tenant_id):
    c_client = cinderclient(request)
    quotas = c_client.quotas.get(tenant_id)
    return quotas

def volume_snapshot_list(request, search_opts=None):
    c_client = cinderclient(request)
    snapshots = []
    for s in c_client.volume_snapshots.list(search_opts=search_opts):
        snapshots.append(s)
    return snapshots
