from django.conf import settings
from django.core.cache import cache
from novaclient import client as nova_client
from novmms.api import base, utils

# when is_admin==True, request can be None
def novaclient(request, is_admin=False):
    insecure = getattr(settings, 'OPENSTACK_SSL_NO_VERIFY', False)
    cacert = getattr(settings, 'OPENSTACK_SSL_CACERT', None)
    if not is_admin:
        auth_url = utils.url_for(request, service_type = 'compute')
        c = nova_client.Client(getattr(settings, 'OPENSTACK_API_VERSION',
                                       {}).get('compute'),
                               request.user.username,
                               request.user.token.id,
                               project_id=request.user.tenant_id,
                               auth_url=auth_url,
                               insecure=insecure,
                               cacert=cacert,
                               http_log_debug=settings.DEBUG)
        c.client.auth_token = request.user.token.id
        c.client.management_url = auth_url
    else: # Some Api needs admin previlege
        admin_auth_ref = utils.get_admin_auth_ref(request)
        auth_url = utils.admin_url_for(admin_auth_ref, service_type = 'compute')
        if admin_auth_ref:
            c = nova_client.Client(getattr(settings, 'OPENSTACK_API_VERSION',
                                           {}).get('compute'),
                                   admin_auth_ref.username,
                                   admin_auth_ref.auth_token,
                                   project_id=admin_auth_ref.project_id,
                                   auth_url=auth_url,
                                   insecure=insecure,
                                   cacert=cacert,
                                   http_log_debug=settings.DEBUG)
            c.client.auth_token = admin_auth_ref.auth_token
            c.client.management_url = auth_url
    return c

def server_list(request, search_opts=None, all_tenants=False, is_admin=False):
    if search_opts is None:
        search_opts = {}
    if all_tenants:
        search_opts['all_tenants'] = True
    else:
        search_opts['project_id'] = request.user.tenant_id
    c = novaclient(request, is_admin = is_admin)
    servers_list = c.servers.list(True, search_opts)
    if isinstance(servers_list, list):
        return [Server(s, request) for s in servers_list]

def server_get(request, instance_id):
    return Server(novaclient(request).servers.get(instance_id), request)

def server_vnc_console(request, instance_id, console_type='novnc'):
    return novaclient(request).servers.get_vnc_console(
                instance_id, console_type)['console']['url']

class Server(base.APIResourceWrapper):
    """Simple wrapper around novaclient.server.Server.

    Preserves the request info so image name can later be retrieved.
    """
    _attrs = ['addresses', 'attrs', 'id', 'image', 'links',
              'metadata', 'name', 'private_ip', 'public_ip', 'status', 'uuid',
              'image_name', 'VirtualInterfaces', 'flavor', 'key_name', 'fault',
              'tenant_id', 'user_id', 'created', 'OS-EXT-STS:power_state',
              'OS-EXT-STS:task_state', 'OS-EXT-SRV-ATTR:instance_name',
              'OS-EXT-SRV-ATTR:host', 'OS-EXT-AZ:availability_zone',
              'OS-DCF:diskConfig', 'os-extended-volumes:volumes_attached']

    def __init__(self, apiresource, request):
        super(Server, self).__init__(apiresource)
        # self.request = request

    # TODO(gabriel): deprecate making a call to Glance as a fallback.
#    @property
#    def image_name(self):
#        import glanceclient.exc as glance_exceptions  # noqa
#        from novmms.api import glance  # noqa
#
#        if not self.image:
#            return _("-")
#        if hasattr(self.image, 'name'):
#            return self.image.name
#        if 'name' in self.image:
#            return self.image['name']
#        else:
#            try:
#                image = glance.image_get(self.request, self.image['id'])
#                return image.name
#            except (glance_exceptions.ClientException,
#                    horizon_exceptions.ServiceCatalogException):
#                return _("-")

    @property
    def internal_name(self):
        return getattr(self, 'OS-EXT-SRV-ATTR:instance_name', "")

    @property
    def availability_zone(self):
        return getattr(self, 'OS-EXT-AZ:availability_zone', "")

    @availability_zone.setter
    def availability_zone(self, v):
        return setattr(self._apiresource, 'OS-EXT-AZ:availability_zone', v)

    @property
    def host_server(self):
        return getattr(self, 'OS-EXT-SRV-ATTR:host', '')

def flavor_list(request, is_public=True, get_extras=False, is_admin=False):
    """Get the list of available instance sizes (flavors)."""
    if not is_admin and cache.get('full_flavors'):
        return cache.get('full_flavors')
    elif is_admin and cache.get('full_flavors_admin'):
        return cache.get('full_flavors_admin')
    else:
        try:
            c = novaclient(request, is_admin = is_admin)
            flavors = c.flavors.list(is_public=is_public)
        except Exception:
            return {}
        full_flavors = dict([(str(flavor.id), flavor.to_dict()) for flavor in flavors])
        if not is_admin:
            cache.set('full_flavors', full_flavors)
        else:
            cache.set('full_flavors_admin', full_flavors)
        return full_flavors
#     if get_extras:
#         for flavor in flavors:
#             flavor.extras = flavor_get_extras(request, flavor.id, True, flavor)
#    return flavors

def flavor_get(request, flavor_id, get_extras=False, is_admin=False):
    if not is_admin and cache.get('full_flavors'):
        return cache.get('full_flavors').get(flavor_id, {})
    elif is_admin and cache.get('full_flavors_admin'):
        return cache.get('full_flavors_admin').get(flavor_id, {})
    else:
        c = novaclient(request, is_admin = is_admin)
        flavor = c.flavors.get(flavor_id)
    #     if get_extras:
    #        flavor.extras = flavor_get_extras(request, flavor.id,
    #                                          True, flavor)
        return flavor.to_dict()

from novaclient.v2 import servers as nova_servers
def server_reboot(request, instance_id, soft_reboot=False):
    hardness = nova_servers.REBOOT_HARD
    if soft_reboot:
        hardness = nova_servers.REBOOT_SOFT
    novaclient(request).servers.reboot(instance_id,
                                           hardness)

def server_stop(request, instance_id):
    novaclient(request).servers.stop(instance_id)

def server_start(request, instance_id):
    novaclient(request).servers.start(instance_id)

def server_delete(request, instance):
    novaclient(request).servers.delete(instance)

def availability_zone_list(request, detailed=False):
    if cache.get('availzones'):
        return cache.get('availzones')
    result = novaclient(request).availability_zones.list(detailed=detailed)
    availzones = [u.zoneName for u in result]
    cache.set('availzones', availzones)
    return availzones

def server_create(request, name, image, flavor, key_name, user_data,
                  security_groups, block_device_mapping=None,
                  block_device_mapping_v2=None, nics=None,
                  scheduler_hints=None,
                  availability_zone=None, instance_count=1, admin_pass=None,
                  disk_config=None, config_drive=None, meta=None):
    return Server(novaclient(request).servers.create(
        name, image, flavor, userdata=user_data,
        security_groups=security_groups,
        key_name=key_name, block_device_mapping=block_device_mapping,
        block_device_mapping_v2=block_device_mapping_v2,
        nics=nics, availability_zone=availability_zone,
        scheduler_hints=scheduler_hints,
        min_count=instance_count, admin_pass=admin_pass,
        disk_config=disk_config, config_drive=config_drive,
        meta=meta), request)

def zone_host_map(request):
    if cache.get('zone_host_map'):
        return cache.get('zone_host_map')
    hosts = novaclient(request).hosts.list()
    ret = {}
    for h in hosts:
        if h.service == 'compute':
            if h.zone in ret:
                ret[h.zone].append(h.host_name)
            else:
                ret[h.zone] = [h.host_name]
    cache.set('zone_host_map', ret)
    return ret

def instance_volume_attach(request, volume_id, instance_id, device=None):
    return novaclient(request).volumes.create_server_volume(instance_id,
                                                            volume_id,
                                                            device)

def instance_volume_detach(request, instance_id, att_id):
    return novaclient(request).volumes.delete_server_volume(instance_id,
                                                            att_id)

def instance_usage(request, reserved=False):
    limits = novaclient(request).limits.get(reserved=reserved).absolute
    limits_dict = {}
    for limit in limits:
        if limit.value < 0:
            if limit.name.startswith('total') and limit.name.endswith('Used'):
                limits_dict[limit.name] = 0
            else:
                limits_dict[limit.name] = float("inf")
        else:
            limits_dict[limit.name] = limit.value
    return limits_dict

def tenant_quota_get(request, tenant_id):
    quotas = novaclient(request).quotas.get(tenant_id)
    return quotas

def keypair_list(request):
    return novaclient(request).keypairs.list()

def keypair_create(request, name):
    return novaclient(request).keypairs.create(name)

def server_migrate(request, instance_id, host=None):
    novaclient(request).servers.migrate(instance_id, host)

def server_confirm_resize(request, instance_id):
    novaclient(request).servers.confirm_resize(instance_id)

def interface_attach(request,
                     server, port_id=None, net_id=None, fixed_ip=None):
    return novaclient(request).servers.interface_attach(server,
                                                        port_id,
                                                        net_id,
                                                        fixed_ip)


def interface_detach(request, server, port_id):
    return novaclient(request).servers.interface_detach(server, port_id)
