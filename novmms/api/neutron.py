from django.conf import settings
from neutronclient.v2_0 import client as neutron_client
from novmms.api import utils

def neutronclient(request):
    insecure = getattr(settings, 'OPENSTACK_SSL_NO_VERIFY', False)
    cacert = getattr(settings, 'OPENSTACK_SSL_CACERT', None)
    auth_url = utils.url_for(request, service_type = 'identity')
    endpoint_url = utils.url_for(request, service_type = 'network')
    c = neutron_client.Client(token=request.user.token.id,
                              auth_url=auth_url,
                              endpoint_url=endpoint_url,
                              insecure=insecure,
                              ca_cert=cacert)
    return c

def subnet_list(request, **params):
    subnets = neutronclient(request).list_subnets(**params).get('subnets')
    return subnets

def network_list(request, **params):
    networks = neutronclient(request).list_networks(**params).get('networks')
    # Get subnet list to expand subnet info in network list.
    subnets = subnet_list(request)
    subnet_dict = dict([(s['id'], s) for s in subnets])
    # Expand subnet list from subnet_id to values.
    for n in networks:
        # Due to potential timing issues, we can't assume the subnet_dict data
        # is in sync with the network data.
        n['subnets'] = [subnet_dict[s] for s in n.get('subnets', []) if
                        s in subnet_dict]
    return networks

def network_list_for_tenant(request, tenant_id, include_external=False,
                            **params):
    if 'networks' in request.session:
        return request.session['networks']

    try:
        networks = network_list(request, tenant_id=tenant_id,
                                shared=False,
                                **params)
        networks += network_list(request, shared=True, **params)
    except Exception:
        return {}
    ret = dict([(n['id'], n) for n in networks])
    request.session['networks'] = ret
    return ret

def port_create(request, network_id, subnet_id, ip_addr=None):
    body = {'port': {'network_id': network_id}}
    kwargs = {}
    kwargs['tenant_id'] = request.user.project_id
    kwargs['fixed_ips'] = [{'subnet_id': subnet_id}]
    if ip_addr:
        kwargs['fixed_ips'][0]['ip_address'] = ip_addr
    body['port'].update(kwargs)
    port = neutronclient(request).create_port(body=body).get('port')
    return port

def port_list(request, **params):
    ports = neutronclient(request).list_ports(**params).get('ports')
    return [p for p in ports]
