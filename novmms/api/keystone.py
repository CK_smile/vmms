#coding=utf-8
from django.conf import settings
from django.core.cache import cache
from keystoneclient.v3 import client as keystone_client_v3
from keystoneclient.v2_0 import client as keystone_client_v2
from novmms.api import utils
from keystoneclient import exceptions as keystone_exceptions

KEYSTONE_CLIENT_VERSION = {
    '2': keystone_client_v2,
    '3': keystone_client_v3,
}

# if admin=True, request can be None
def keystoneclient(request, admin=False, version='3'):
    # 在本次请求中缓存该keystoneclient(由于一次请求中可能会多次使用client)
    # 使用缓存后，v2/v3的client都会请求auth_token，会不会相互影响（新token屏蔽了老token）
    # 从实践来看，该token不会互相屏蔽
    cache_attr = "_keystoneclient_admin" if admin \
    else '_keystoneclient'
    cache_attr = version + cache_attr
    if request and hasattr(request, cache_attr):
#    and (not request.user.token.id or
#         getattr(request, cache_attr).auth_token == user.token.id)):
        conn = getattr(request, cache_attr)
    else:
        insecure = getattr(settings, 'OPENSTACK_SSL_NO_VERIFY', False)
        cacert = getattr(settings, 'OPENSTACK_SSL_CACERT', None)
        if not request or admin:
            admin_auth_ref = utils.get_admin_auth_ref(request)
            auth_url = utils.admin_url_for(admin_auth_ref, service_type = 'identity')
            original_ip = request.environ.get('REMOTE_ADDR', '') if request \
            else None
            conn = KEYSTONE_CLIENT_VERSION[version].Client(token=admin_auth_ref.auth_token,
                                             endpoint=auth_url,
                                             original_ip=original_ip,
                                             insecure=insecure,
                                             cacert=cacert,
                                             auth_url=auth_url,
                                             debug=settings.DEBUG)
        else:
            auth_url = utils.url_for(request, service_type = 'identity')
            conn = KEYSTONE_CLIENT_VERSION[version].Client(token=request.user.token.id,
                                             endpoint=auth_url,
                                             original_ip=request.environ.get('REMOTE_ADDR', ''),
                                             insecure=insecure,
                                             cacert=cacert,
                                             auth_url=auth_url,
                                             debug=settings.DEBUG)
        if request:
            setattr(request, cache_attr, conn)
    return conn

def username_list(request, project=None, domain=None, group=None, filters=None):
    kwargs = {
        "tenant_id": project,
    }
    # use version 2, version 3 can't use project_id (why)
    users = keystoneclient(request, admin=True, version='2').users.list(**kwargs)
    return [user.name for user in users]

from openstackclient.common import utils as openstackclient_utils
def user_create(request, name=None, email=None, password=None, project=None,
                role=None, enabled=True, domain=None, description=None):
    try:
        # 这种v3的find_resource的方法，总报告找不到NotFound（是由于get_admin_auth_ref使用了v2的keystone吗？）
        # 所以，这里使用了v2
        manager_v2 = keystoneclient(request, admin=True,version='2')
        project = openstackclient_utils.find_resource(
            manager_v2.tenants,
            project)
        role = openstackclient_utils.find_resource(
            manager_v2.roles,
            role)
        member_role = openstackclient_utils.find_resource(
            manager_v2.roles,
            '_member_')
        manager = keystoneclient(request, admin=True)
        user = manager.users.create(name, password=password, email=email,
                                    default_project=project.id,
                                    enabled=enabled,
                                    domain=domain,
                                    description=description)
        # 为用户添加角色 自身角色stuff/leader 和 os默认角色 _member_
        role = manager_v2.roles.add_user_role(user, role.id, project.id)
        role = manager_v2.roles.add_user_role(user, member_role.id, project.id)
    except Exception:
        raise
#    这种v3的设置角色的方法，总报告找不到NotFound
#    role = manager.roles.grant(role.id, user=name, project=project.id,
#                               group=None,
#                               domain=domain)

def tenant_list(request=None, domain=None,
                admin=True, filters=None):
    if cache.get('tenant_dict'):
        return cache.get('tenant_dict')
    else:
        manager_v2 = keystoneclient(request, admin=admin, version='2').tenants
        tenants = manager_v2.list()
        tenant_dict = dict([(t.id, t.name) for t in tenants])
        cache.set('tenant_dict', tenant_dict)
        return tenant_dict
