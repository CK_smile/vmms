from django.conf import settings
from openstack_auth import utils as os_auth_utils
from novmms.exceptions import AuthFailed
from django.core.cache import cache
from keystoneauth1.identity import v2 as v2_auth

POWER_STATES = {
        0: "no state",
        1: "running",
        2: "blocked",
        3: "paused",
        4: "shutdown",
        5: "shutoff",
        6: "crashed",
        7: "suspended",
        8: "failed",
        9: "building",
}

def get_power_state(instance):
    return POWER_STATES.get(getattr(instance, "OS-EXT-STS:power_state", 0),
                            '').lower()

def format_addresses(addr):
    """
    'addresses': {
        u'selfservice': [
            {
                u'OS-EXT-IPS-MAC: mac_addr': u'fa: 16: 3e: 34: 7c: ef',
                u'version': 4,
                u'addr': u'172.99.1.15',
                u'OS-EXT-IPS: type': u'fixed'
            },
            {
                u'OS-EXT-IPS-MAC: mac_addr': u'fa: 16: 3e: c4: 02: a1',
                u'version': 4,
                u'addr': u'172.99.1.17',
                u'OS-EXT-IPS: type': u'fixed'
            }
        ]
    },
    """
   # ret = []
   # for (network, ips) in addr.items():
   #     data = {}
   #     data['network'] = network
   #     data['ips'] = []
   #     for ip in ips:
   #         data['ips'].append(ip['addr'])
   #     ret.append(data)
   # return ret
    ret = []
    for (network, ips) in addr.items():
        for ip in ips:
            ret.append(ip['addr'])
    return ','.join(ret)

def url_for(request, service_type, url_type='publicURL'):
    sc = request.user.token.serviceCatalog
    for endpoint in sc:
        if endpoint['type'] == service_type:
            return endpoint['endpoints'][0][url_type]

def admin_url_for(auth_ref, service_type, url_type='adminURL'):
    try:
        returl = auth_ref.service_catalog.url_for(service_type=service_type,
                                                  interface=url_type)
    except Exception:
        return None
    return returl

def get_admin_auth_ref(request):
    if not request:
        region_url = getattr(settings, 'AVAILABLE_REGIONS', [])[0][0]
    else:
        region_url = request.POST.get('region', request.POST.get('region',
                                                                 getattr(settings,
                                                                        'AVAILABLE_REGIONS',
                                                                        [])[0][0]))
    admin_info = getattr(settings, 'ADMIN_ACCOUNT', {})[region_url]
    auth_plugin = v2_auth.Password(auth_url=admin_info['auth_url'],
                     username=admin_info['username'],
                     password=admin_info['password'],
                     tenant_name=admin_info['tenant_name'])
    session = os_auth_utils.get_session()
    try:
        auth_ref = auth_plugin.get_access(session)
    except Exception as exc:
        return None
    return auth_ref

