import functools
import json

from django import http
from django.utils import decorators

from novmms.exceptions import AjaxError

class _RestResponse(http.HttpResponse):
    @property
    def json(self):
        content_type = self['Content-Type']
        if content_type != 'application/json':
            raise ValueError("content type is %s" % content_type)
        body = self.content.decode('utf-8')
        return json.loads(body)

class CreatedResponse(_RestResponse):
    def __init__(self, location, data=None):
        if data is not None:
            content = json.dumps(data)
            content_type = 'application/json'
        else:
            content = ''
            content_type = None
        super(CreatedResponse, self).__init__(status=201, content=content,
                                              content_type=content_type)
        self['Location'] = location

class JSONResponse(_RestResponse):
    def __init__(self, data, status=200):
        if status == 204:
            content = ''
        else:
            content = json.dumps(data)

        super(JSONResponse, self).__init__(
            status=status,
            content=content,
            content_type='application/json',
        )

def ajax(perm=None, authenticated=True, data_required=False):
    '''copy from openstack_dashboard/api/rest/utils.py
    '''
    def decorator(function, authenticated=authenticated,
                  data_required=data_required, perm=perm):
        @functools.wraps(function,
                         assigned=decorators.available_attrs(function))
        def _wrapped(self, request, *args, **kw):
            if authenticated and not request.user.is_authenticated():
                return JSONResponse('not logged in', 401)
            if perm and not request.user.has_perms(perm):
                return JSONResponse('unanthorized', 403)
            if not request.is_ajax():
                return JSONResponse('request must be AJAX', 400)

            # decode the JSON body if present
            request.DATA = None
            if request.body:
                try:
                    request.DATA = json.loads(request.body)
                except (TypeError, ValueError) as e:
                    # request.body='keypair_name=wagaga&csrfmiddlewaretoken=wLEA...
                    request.DATA = dict(request.POST.items())
                    if not request.DATA:
                        return JSONResponse('malformed JSON request: %s' % e, 400)

            if data_required:
                if not request.DATA:
                    return JSONResponse('request requires JSON body', 400)

            # invoke the wrapped function, handling exceptions sanely
            try:
                data = function(self, request, *args, **kw)
                if isinstance(data, http.HttpResponse):
                    return data
                elif data is None:
                    return JSONResponse('', status=204)
                return JSONResponse(data)
            except AjaxError as e:
                # exception was raised with a specific HTTP status
                for attr in ['http_status', 'code', 'status_code']:
                    if hasattr(e, attr):
                        http_status = getattr(e, attr)
                        break
                else:
                    return JSONResponse(str(e), 500)
                return JSONResponse(str(e), http_status)
            except Exception as e:
                return JSONResponse(str(e), 500)

        return _wrapped
    return decorator
