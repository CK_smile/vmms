#coding=utf-8
from django.views import generic

from django.http import HttpResponseRedirect, HttpResponse
from django.template.defaultfilters import slugify
from django.utils import http as utils_http
from novmms import api
from novmms.api.rest import urls
from novmms.api.rest import utils as rest_utils
from novmms.exceptions import AjaxError

from novmms.dashboard.instances.models import Instance
from novmms.dashboard.instances.forms import LaunchInstanceForm, SetIPForm

from novmms.dashboard.volumes.forms import CreateVolumeForm

from django.conf import settings
PERMISSION_REQUIRED = settings.PERMISSION_REQUIRED

@urls.register
class Regions(generic.View):
    url_regex = r'regions/$'

    @rest_utils.ajax(authenticated = False)
    def get(self, request):
        default_region = {'url':settings.OPENSTACK_KEYSTONE_URL, 'name':"Default Region"}
        regions = getattr(settings, 'AVAILABLE_REGIONS', [])
        if not regions:
            regionList = [default_region]
        else:
            regionList = [dict([('url', r[0]), ('name', r[1])]) for r in regions]
        return {'items': regionList}

@urls.register
class Users(generic.View):
    """API for retrieving user_list
    """
    url_regex = r'users/$'

    @rest_utils.ajax(PERMISSION_REQUIRED['admin'])
    def get(self, request):
        """Get a specific project's users. Example:

        """
        if request.user.has_perm('openstack.roles.admin'):
            project = None
        else:
            project = request.user.tenant_id
        u_list = api.keystone.username_list(request, project=project)
        return {'items': [{'key': u, 'val': u} for u in u_list]}

@urls.register
class Servers(generic.View):
    """API over all servers.
    """
    url_regex = r'servers/$'

    @rest_utils.ajax(PERMISSION_REQUIRED['admin'])
    def get(self, request):
        """Get a list of servers.

        The listing result is an object with property "items". Each item is
        a server.

        Example GET:
        http://localhost/api/nova/servers
        """
        servers = api.nova.server_list(request)
        return {'items': [{'key': s.name, 'val': s.id} for s in servers]}

    @rest_utils.ajax(PERMISSION_REQUIRED['admin'], data_required=True)
    def post(self, request):
        """Create new instance.
        """
        form = LaunchInstanceForm(request, request.DATA)
        if form.is_valid():
            msg = 'ok'
            data = form.cleaned_data
            count = data['count'] if data['count'] else 1
            nics = []
            hints = None
            for p in data['port_id'].split(','):
                nics.append({'port-id': p})
            if data['host'] and data['same_host']:
                msg = 'host and same_host cant be set at same time'
            if data['host']:
                try:
                    host_list = api.nova.zone_host_map(request).get(data['availzones'], [])
                except Exception as exc:
                    raise AjaxError(500, str(exc))
                if data['host'] not in host_list:
                    msg = 'Failed:host not in AZ'
                else:
                    data['availzones'] += ':' + data['host']
            if data['same_host']:
                hints = {'same_host': data['same_host']}
            if msg == 'ok':
                try:
                    # call nova to create server
                    instance = api.nova.server_create(request,
                                                      data['name'],
                                                      data['image'],
                                                      data['flavor'],
                                                      data['keypair_name'],
                                                      None,
                                                      ['default'],
                                                      block_device_mapping=None,
                                                      block_device_mapping_v2=None,
                                                      nics=nics,
                                                      availability_zone=data['availzones'],
                                                      scheduler_hints=hints,
                                                      instance_count=count,
                                                      admin_pass=None,
                                                      disk_config=None,
                                                      config_drive=None)
                except Exception as exc:
                    raise AjaxError(500, str(exc))
                try:
                    # create this Instance in db
                    Instance.objects.create(uuid=instance.id,
                                            addresses=api.utils.format_addresses(instance.addresses),
                                            name=data['name'],
                                            key_name=data['keypair_name'],
                                            status=instance.status,
                                            power_state=api.utils.get_power_state(instance),
                                            host=data['host'],# api.nova.get_host(instance.id),
                                            availability_zone=data['availzones'],
                                            launched_at=instance.created,
                                            flavor=api.nova.flavor_list(request)[data['flavor']]['name'],
                                            image=api.glance.image_list_detailed(request)[data['image']]['name'],
                                            project=instance.tenant_id,
                                            owner=data['owner'])
                except Exception as exc:
                    raise AjaxError(500, 'Instance Create OK, Error when \
                                    write to DB. '+str(exc))
        else:
            raise AjaxError(400, 'Invalid Post Data')
        return {'item': instance.to_dict()}

# copy from dashboard/instances/views.py
# [TODO] put it to utils.py
ACTIVE_STATES = ("ACTIVE",)
def is_deleting(instance):
    task_state = getattr(instance, "OS-EXT-STS:task_state", None)
    if not task_state:
        return False
    return task_state.lower() == "deleting"
@urls.register
class Server(generic.View):
    """API for retrieving a single server
    """
    url_regex = r'servers/(?P<server_id>[^/]+|default)/$'

    @rest_utils.ajax(PERMISSION_REQUIRED['user'])
    def get(self, request, server_id):
        """Get a specific server. Example:

        """
        dataObj = Instance.objects.get(uuid=server_id)
        if request.user.has_perm('openstack.roles.leader') or dataObj.owner == request.user.username:
            instance = api.nova.server_get(request, server_id)
            instance.addresses = api.utils.format_addresses(instance.addresses)
            instance.power_state = api.utils.get_power_state(instance)
            instance.uuid = instance.id
            dataObj.addresses = instance.addresses
            dataObj.status = instance.status
            dataObj.power_state = instance.power_state
            dataObj.save()
            instance.owner = dataObj.owner
            instance.modified = dataObj.modified
            instance.availability_zone = dataObj.availability_zone
            ret = instance.to_dict()
            return ret
        else:
            return{'error': 'unauthorized'}

    @rest_utils.ajax(PERMISSION_REQUIRED['user'], data_required=True)
    def post(self, request, server_id):
        instance_id = server_id
        action = request.DATA.get('action')
        if not action:
            raise AjaxError(400, 'Invalid Post Data: missed action')
        if action == 'reboot': # reboot
            try:
                instance = api.nova.server_get(request, instance_id)
                if instance is not None:
                    if ((instance.status in ACTIVE_STATES
                         or instance.status == 'SHUTOFF')
                        and not is_deleting(instance)):
                        api.nova.server_reboot(request, instance_id, soft_reboot=False)
            except Exception as exc:
                raise AjaxError(500, str(exc))
        elif action == 'soft_reboot': # soft_reboot
            try:
                api.nova.server_reboot(request, instance_id, soft_reboot=True)
            except Exception as exc:
                raise AjaxError(500, str(exc))
        elif action == 'shutoff': # shutoff
            try:
                instance = api.nova.server_get(request, instance_id)
                if instance is not None:
                    if ((api.utils.get_power_state(instance) in ("running", "suspended"))
                        and not is_deleting(instance)):
                        api.nova.server_stop(request, instance_id)
            except Exception as exc:
                raise AjaxError(500, str(exc))
        elif action == 'start': # start
            try:
                instance = api.nova.server_get(request, instance_id)
                if instance is not None:
                    if instance.status in ("SHUTDOWN", "SHUTOFF", "CRASHED"):
                        api.nova.server_start(request, instance_id)
            except Exception as exc:
                raise AjaxError(500, str(exc))
        elif action == 'migrate': # migrate
            if not request.user.has_perm('openstack.roles.leader'):
                raise AjaxError(403, 'Not authorized')
            host = request.DATA.get('data').get('host')
            if host:
                try:
                    api.nova.server_migrate(request, instance_id, host)
                    # api.nova.server_confirm_resize(request, instance_id)
                except Exception as exc:
                    raise AjaxError(500, str(exc))
            else:
                raise AjaxError(400, 'Invalid Post Data')
        else: # unsupported action
            raise AjaxError(400, 'Action %s Unsupported' % action)
        try:
            dataObj = Instance.objects.get(uuid=instance_id)
            # [TODO] update in DB
        except Exception as exc:
            raise AjaxError(500, '%s OK, but Failed to update \
                            DB.%s' % (action, str(exc)))
        return {'result': 'ok'}

    @rest_utils.ajax(PERMISSION_REQUIRED['admin'])
    def delete(self, request, server_id):
        instance_id = server_id
        try:
            api.nova.server_delete(request, instance_id)
        except Exception as exc:
            raise AjaxError(500, str(exc))
        try:
            Instance.objects.get(uuid=instance_id).delete()
        except Exception as exc:
            raise AjaxError(500, 'deleted ok, but failed when delete from DB. %s'
                            % str(exc))
        return {'result': 'ok'}

@urls.register
class Ports(generic.View):
    """API for Neutron Ports
    http://developer.openstack.org/api-ref-networking-v2.html#ports
    """
    url_regex = r'ports/$'

    @rest_utils.ajax(PERMISSION_REQUIRED['admin'])
    def get(self, request):
        """Get a list of ports for the current user's access

        The listing result is an object with property "items".  Each item is
        a subnet.
        """
        # see
        # https://github.com/openstack/neutron/blob/master/neutron/api/v2/attributes.py
        result = api.neutron.port_list(request)
        ports = [dict([('val', p['id']), ('key', [ip['ip_address'] for ip in
                                                  p['fixed_ips']])]) for p in
                                                    result if p['status']=='DOWN']
        return{'items': ports}

    @rest_utils.ajax(PERMISSION_REQUIRED['admin'], data_required=True)
    def post(self, request):
        ports = []
        form = SetIPForm(request, request.DATA)
        if form.is_valid():
            count = form.cleaned_data.get('ip_count')
            count = count if count else 1
            ip_addr = form.cleaned_data['ip_addr']
            availzone = form.cleaned_data['availzones']
            try:
                network = api.neutron.network_list_for_tenant(request,
                                                              request.user.tenant_id).get(form.cleaned_data['networks'], None)
            except Exception as exc:
                raise AjaxError(500, str(exc))
            if network:
                # 查找选择的网络中包含此可用域对应的subnet
                subnets = dict([(sub['name'], sub) for sub in network['subnets']])
                for netname in getattr(settings, 'AVAILZONES_SUBNET_MAPPING', {})[availzone]:
                    if netname in subnets: # 此时，进行ip分配
                        subnetinfo = subnets[netname]
                        try:
                            if ip_addr != '':
                                ports.append(api.neutron.port_create(request,
                                                                    network_id=network['id'],
                                                                    subnet_id=subnetinfo['id'],
                                                                    ip_addr=ip_addr))
                            else:
                                for i in range(count):
                                    ports.append(api.neutron.port_create(request,
                                                                        network_id=network['id'],
                                                                        subnet_id=subnetinfo['id']))
                        except Exception as exc:
                            if ports: # 只分配了一部分, 删除掉(只允许整体分配)
                                pass
                            raise AjaxError(500, str(exc))
                if not ports: # 可用域中不支持所选择的网络(不应该发生)
                    raise AjaxError(400, 'Network %s Not Supported In %s \
                                    availzone' % (network['name'],
                                                  availzone))
        else:
            raise AjaxError(400, 'Invalid Post Data')
        result = [dict([('val', p['id']), ('key', [ip['ip_address'] for ip in
                                                  p['fixed_ips']])]) for p in
                                                    ports if p['status']=='DOWN']
        return{'items': result}

@urls.register
class AvailabilityZones(generic.View):
    """API for nova availability zones.
    """
    url_regex = r'availzones/$'

    @rest_utils.ajax(PERMISSION_REQUIRED['user'])
    def get(self, request):
        """Get a list of availability zones.

        The following get parameters may be passed in the GET
        request:

        :param detailed: If this equals "true" then the result will
            include more detail.

        The listing result is an object with property "items".
        """
        detailed = request.GET.get('detailed') == 'true'
        result = api.nova.availability_zone_list(request, detailed)
        #return {'items': result}
        return {'items': [{'key': u, 'val': u} for u in result]}

@urls.register
class Networks(generic.View):
    """API for Neutron Networks

    http://developer.openstack.org/api-ref-networking-v2.html
    """
    url_regex = r'networks/$'

    @rest_utils.ajax(PERMISSION_REQUIRED['admin'])
    def get(self, request):
        """Get a list of networks for a project

        The listing result is an object with property "items".  Each item is
        a network.
        """
        tenant_id = request.user.tenant_id
        result = api.neutron.network_list_for_tenant(
            request, tenant_id, include_external=True)
        return{'items': [dict([('key', n['name']), ('val', n['id'])])
                         for n in result.values()]}

@urls.register
class Keypairs(generic.View):
    """API for nova keypairs.
    """
    url_regex = r'keypairs/$'

    @rest_utils.ajax(PERMISSION_REQUIRED['user'])
    def get(self, request):
        """Get a list of keypairs associated with the current logged-in
        account.

        The listing result is an object with property "items".
        """
        result = api.nova.keypair_list(request)
        return {'items': [{'key':k.name, 'val':k.name} for k in result]}

    @rest_utils.ajax(PERMISSION_REQUIRED['admin'], data_required=True)
    def post(self, request):
        name = request.DATA.get('keypair_name', None)
        try:
            keypair = api.nova.keypair_create(request, name)
            msg = keypair.private_key
            response = HttpResponse(content_type='application/binary')
            response['Content-Disposition'] = ('attachment; filename=%s.pem'
                                               % slugify(name))
            response.write(keypair.private_key)
            response['Content-Length'] = str(len(response.content))
            return response
        except Exception as exc:
            raise AjaxError(500, str(exc))

@urls.register
class Hosts(generic.View):
    """API for nova keypairs.
    """
    url_regex = r'hosts/(?P<availzone>[^/]+|default)/$'

    @rest_utils.ajax(PERMISSION_REQUIRED['admin'])
    def get(self, request, availzone):
        """Get a list of hosts associated with the current logged-in
        account.

        The listing result is an object with property "items".
        """
        result = api.nova.zone_host_map(request)
        hosts = []
        if availzone == 'default': # return all hosts
            for h in result.values():
                hosts += h
        else: # return the specific zone's hosts
            hosts = result.get(availzone, [])
            return {'items': [{'key': h, 'val': h} for h in hosts]}

@urls.register
class Flavors(generic.View):
    """API for nova flavors.
    """
    url_regex = r'flavors/$'

    @rest_utils.ajax(PERMISSION_REQUIRED['user'])
    def get(self, request):
        """Get a list of flavors.
        """
        flavors = [dict([('id', fid), ('name', f['name']), ('ram', f['ram']),
                         ('vcpus', f['vcpus']), ('disk', f['disk'])]) for fid,
                    f in api.nova.flavor_list(request).items()]
        return {'items': flavors}

@urls.register
class Flavor(generic.View):
    """API for retrieving a single flavor
    """
    url_regex = r'flavors/(?P<flavor_id>[^/]+)/$'

    @rest_utils.ajax(PERMISSION_REQUIRED['user'])
    def get(self, request, flavor_id):
        """Get a specific flavor
        """
        result = api.nova.flavor_get(request, flavor_id)
        return result

@urls.register
class Images(generic.View):
    """API for Glance images.
    """
    url_regex = r'images/$'

    @rest_utils.ajax(PERMISSION_REQUIRED['user'])
    def get(self, request):
        """Get a list of images.
        """
        images = [dict([('val', iid), ('key', i['name'])]) for iid, i in
         api.glance.image_list_detailed(request).items()]
        return{'items': images}

@urls.register
class Image(generic.View):
    """API for retrieving a single image
    """
    url_regex = r'images/(?P<image_id>[^/]+|default)/$'

    @rest_utils.ajax(PERMISSION_REQUIRED['user'])
    def get(self, request, image_id):
        """Get a specific image

        http://localhost/api/glance/images/cc758c90-3d98-4ea1-af44-aab405c9c915
        """
        return api.glance.image_get(request, image_id)

@urls.register
class Interfaces(generic.View):
    """API for nova keypairs.
    """
    url_regex = r'interfaces/(?P<instance_id>[^/]+|default)/$'

    @rest_utils.ajax(PERMISSION_REQUIRED['admin'])
    def get(self, request, instance_id):
        """Get a list of keypairs associated with the current logged-in
        account.

        The listing result is an object with property "items".
        """
        result = api.neutron.port_list(request, device_id=instance_id)
        ports = [dict([('val', p['id']), ('key', [ip['ip_address'] for ip in
                                                  p['fixed_ips']])]) for p in
                                                  result]
        return{'items': ports}

    @rest_utils.ajax(PERMISSION_REQUIRED['admin'], data_required=True)
    def post(self, request, instance_id):
        port_id = request.DATA.get('port_id')
        if instance_id and port_id:
            try:
                api.nova.interface_attach(request, instance_id, port_id)
            except Exception as exc:
                raise AjaxError(500, str(exc))
            else:
                try:
                    dataObj = Instance.objects.get(uuid=instance_id)
                    # [TODO] update in DB
                except Exception as exc:
                    raise AjaxError(500, 'detach OK, but Failed to update \
                                    DB.%s' % str(exc))
                return {'result': 'ok'}
        else:
            raise AjaxError(400, 'Invalid Post Data')

    @rest_utils.ajax(PERMISSION_REQUIRED['admin'], data_required=True)
    def delete(self, request, instance_id):
        port_id = request.DATA.get('port_id')
        if instance_id and port_id:
            try:
                api.nova.interface_detach(request, instance_id, port_id)
            except Exception as exc:
                raise AjaxError(500, str(exc))
            else:
                try:
                    dataObj = Instance.objects.get(uuid=instance_id)
                    # [TODO] update in DB
                except Exception as exc:
                    raise AjaxError(500, 'detach OK, but Failed to update \
                                    DB.%s' % str(exc))
                return {'result': 'ok'}
        else:
            raise AjaxError(400, 'Invalid Post Data')

@urls.register
class Volumes(generic.View):
    """API for cinder volumes.
    """
    url_regex = r'volumes/$'

    @rest_utils.ajax(PERMISSION_REQUIRED['user'])
    def get(self, request):
        """Get a list of keypairs associated with the current logged-in
        account.

        The listing result is an object with property "items".
        """
        try:
            volumes = api.cinder.volume_list(request)
        except Exception as exc:
            raise AjaxError(500, str(exc))
        return {'items': [{'key':v.id, 'val':v.name} for v in volumes]}

    @rest_utils.ajax(PERMISSION_REQUIRED['admin'], data_required=True)
    def post(self, request):
        form = CreateVolumeForm(request, request.DATA)
        if form.is_valid():
            data = form.cleaned_data
            hints = None
            if not data['snapshot_source']:
                data['snapshot_source'] = None
            if data['local_to_instance']:
                hints = {'local_to_instance': data['local_to_instance']}
            if data.get("snapshot_source"):
                # Create from Snapshot
                snapshot = api.cinder.volume_snapshot_get(request,
                                                          data["snapshot_source"])
                snapshot_id = snapshot.id
                if (data['size'] < snapshot.size):
                    raise AjaxError(400, 'The volume size cannot be less than \
                                    the snapshot size (%sGiB)' % snapshot.size)
            try:
                volume = api.cinder.volume_create(request,
                                                  data['size'],
                                                  data['name'],
                                                  '', # desc
                                                  '', # type
                                                  availability_zone=data['availzones'],
                                                  snapshot_id=data['snapshot_source'],
                                                  scheduler_hints=hints)
            except Exception as exc:
                raise AjaxError(500, str(exc))
        else:
            raise AjaxError(400, 'Invalid Post Data')
        return {'result': 'ok'}

@urls.register
class Snapshots(generic.View):
    """API for cinder volume's snapshots.
    """
    url_regex = r'snapshots/$'

    @rest_utils.ajax(PERMISSION_REQUIRED['user'])
    def get(self, request):
        """Get a list of keypairs associated with the current logged-in
        account.

        The listing result is an object with property "items".
        """
        try:
            snapshots = api.cinder.volume_snapshot_list(request)
        except Exception as exc:
            raise AjaxError(500, str(exc))
        return {'items': [{'key':s.name, 'val':s.id} for s in snapshots]}

@urls.register
class Volume(generic.View):
    """API for nova keypairs.
    """
    url_regex = r'volumes/(?P<volume_id>[^/]+|default)/$'

    @rest_utils.ajax(PERMISSION_REQUIRED['admin'])
    def get(self, request, volume_id):
        """获取volume当前挂载在哪个机器上
        """
        volume = api.cinder.volume_get(request,
                                       volume_id)
        instances = []
        for att in volume.attachments:
            if att.get('server_id', None):
                server = api.nova.server_get(request, att['server_id'])
                instances.append(server)
        attachments = [dict([('val', i.id), ('key', i.name)])
                       for i in instances]
        return{'items': attachments}

    @rest_utils.ajax(PERMISSION_REQUIRED['user'], data_required=True)
    def post(self, request, volume_id):
        action = request.DATA.get('action')
        if not action:
            raise AjaxError(400, 'Invalid Post Data: missed action')
        if action == 'attach': # reboot
            attach_to = request.DATA.get('data').get('val')
            if attach_to:
                try:
                    api.nova.instance_volume_attach(request, volume_id, attach_to)
                except Exception as exc:
                    raise AjaxError(500, str(exc))
            else:
                raise AjaxError(400, 'Invalid Post Data')
        elif action == 'detach':
            detach_from = request.DATA.get('data').get('val')
            if detach_from:
                try:
                    api.nova.instance_volume_detach(request, detach_from,
                                                    volume_id)
                except Exception as exc:
                    raise AjaxError(500, str(exc))
            else:
                raise AjaxError(400, 'Invalid Post Data')
        elif action == 'snapshot':
            data = request.DATA.get('data')
            name = data.get('name')
            desc = data.get('desc')
            if name and desc:
                force = False
                try:
                    volume = api.cinder.volume_get(request,
                                                   volume_id)
                    if volume.status == 'in-use':
                        force = True
                    api.cinder.volume_snapshot_create(request,
                                                      volume_id,
                                                      name,
                                                      desc,
                                                      force=force)
                except Exception as exc:
                    raise AjaxError(500, str(exc))
            else:
                raise AjaxError(400, 'Invalid Post Data')
        elif action == 'migrate':
            host = request.DATA.get('data').get('host')
            if host:
                try:
                    api.cinder.volume_migrate(request, volume_id, host)
                except Exception as exc:
                    raise AjaxError(500, str(exc))
            else:
                raise AjaxError(400, 'Invalid Post Data')
        else: # unsupported action
            raise AjaxError(400, 'Action %s Unsupported' % action)
        return {'result': 'ok'}

    @rest_utils.ajax(PERMISSION_REQUIRED['admin'], data_required=True)
    def delete(self, request, volume_id):
        """将volume删除
        """
        try:
            api.cinder.volume_delete(request, volume_id)
        except Exception as exc:
            raise AjaxError(500, str(exc))
        return {'result': 'ok'}
