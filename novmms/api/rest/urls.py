from django.conf import urls

urlpatterns = []


# to register the URLs for your API endpoints, decorate the view class with
# @register below, and the import the endpoint module in the
# rest_api/__init__.py module
def register(view):
    '''Register API views to respond to a regex pattern (url_regex on the
    view class).

    The view should be a standard Django class-based view implementing an
    as_view() method. The url_regex attribute of the view should be a standard
    Django URL regex pattern.
    '''
    p = urls.url(view.url_regex, view.as_view())
#    p.add_prefix('openstack_dashboard.rest_api')
    urlpatterns.append(p)
    return view
