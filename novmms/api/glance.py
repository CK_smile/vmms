# Copyright 2012 United States Government as represented by the
# Administrator of the National Aeronautics and Space Administration.
# All Rights Reserved.
#
# Copyright 2012 Nebula, Inc.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
from django.conf import settings
from django.core.cache import cache

import glanceclient as glance_client
from novmms.api import utils

def glanceclient(request, version='1', is_admin=False):
    insecure = getattr(settings, 'OPENSTACK_SSL_NO_VERIFY', False)
    cacert = getattr(settings, 'OPENSTACK_SSL_CACERT', None)
    if is_admin:
        admin_auth_ref = utils.get_admin_auth_ref(request)
        token = admin_auth_ref.auth_token
        url = utils.admin_url_for(admin_auth_ref, service_type = 'image')
    else:
        token = request.user.token.id
        url = utils.url_for(request, service_type = 'image')
    return glance_client.Client(version, url, token=token,
                                insecure=insecure, cacert=cacert)

def image_get(request, image_id):
    """Returns an Image object populated with metadata for image
    with supplied identifier.
    """
    if cache.get('image_map'):
        if cache.get('image_map').get(image_id, None):
            return cache.get('image_map').get(image_id)
    image = glanceclient(request).images.get(image_id)
    if not hasattr(image, 'name'):
        image.name = None
    return image.to_dict()

def image_list_detailed(request, marker=None, sort_dir='desc',
                        sort_key='created_at', filters=None, paginate=False,
                        reversed_order=False, is_admin=False):
    if not is_admin and cache.get('image_map'):
        return cache.get('image_map')
    elif is_admin and cache.get('image_map_admin'):
        return cache.get('image_map_admin')
    else:
        try:
            kwargs = {'filters': filters or {}}
            if marker:
                kwargs['marker'] = marker
            kwargs['sort_key'] = sort_key
            if not reversed_order:
                kwargs['sort_dir'] = sort_dir
            else:
                kwargs['sort_dir'] = 'desc' if sort_dir == 'asc' else 'asc'
            c = glanceclient(request, is_admin = is_admin)
            images_iter = c.images.list(**kwargs)
            images = list(images_iter)
        except Exception:
            return {}
        image_map = dict([(str(image.id), image.to_dict()) for image in images])
        if not is_admin:
            cache.set('image_map', image_map)
        else:
            cache.set('image_map_admin', image_map)
        return image_map
