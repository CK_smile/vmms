var DEBUG = false
/********************
 * common functions
 ********************/
var confirmModal = function(op, clickFunc) {
    $('.modal-body').html(op)
    $('#confirmBtn').prop('disabled', false)
    $('#myModal').modal('show')
    $('#confirmBtn').off('click')
    $('#confirmBtn').one('click', function() {
        $(this).prop('disabled', true)
        $('.modal-body').html('Please Wait......')
        clickFunc($('.modal-body'))
    })
}
var toggleFunc = function(obj) {
	obj.show = !obj.show
};
/********************
 * auth/login.html
 ********************/
var app = angular.module('loginApp', []);
app.config(function($httpProvider, $interpolateProvider) {
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
});
app.controller('loginCtrl', function($scope, $http) {
    $http.get("/api/regions").then(function(response) {
        $scope.myRegions = response.data.items;
    });
});

/********************
 * instances/index.html
 ********************/
var app = angular.module('instancesApp', []);
app.config(function($httpProvider, $interpolateProvider) {
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    // http://stackoverflow.com/questions/18156452/django-csrf-token-angularjs
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
});
app.controller('instancesCtrl', function($scope, $http) {
    $scope.debug = DEBUG
    // helper functions
    var list_add = function(list, item) {
        console.log('port_id_list_add')
        console.log(item)
        list.push(item)
    }
    var list_del = function(list, item) { //must return sth to rewrite original list
        console.log('port_id_list_del')
        console.log(item)
        return list.filter(function(el) {
            return el.val !== item.val;
        });
    }
    $scope.toggle = toggleFunc
    $scope.port_list = function(data, uuid) {
        $http.get("/api/interfaces/" + uuid + "/").then(function(response) {
            data.port_list = response.data.items
        });
    }
    $scope.instanceAction = function(uuid, name, action, senddata) {
	console.log(action)
        console.log(senddata)
        if (typeof(senddata) == "undefined") {
            return
        }
        var urlpre = '/api/servers/'
        tips = '即将对 ' + name + ' 实例进行 ' + action
        confirmModal(tips, function(resultEle) {
            if (action == 'delete') {
                m = 'delete'
            } else {
                m = 'post'
            }
            $http({
                method: m,
                url: urlpre + uuid + '/',
                data: {
                    action: action,
	    	    data: senddata
                } // pass in data as strings
            }).success(function(result) {
                console.log(result)
                resultEle.html(action + ' Success!')
            }).error(function(err) {
                console.log(err)
                resultEle.html(action + ' Error: ' + err)
            })
        })
    }
    $scope.interfaceFormData = {}
    $scope.interfaceFormSubmit = function(obj, field, sendurl, m) {
        console.log(field)
        console.log(typeof(obj[field]))
        if (typeof(obj[field]) == "undefined") {
            return
        }
        if (m == 'post') {
            op = 'Attach'
        } else if (m == 'delete') {
            op = 'Detach'
        }
        var tips = op + ' 网络接口: ' + obj[field].key;
        confirmModal(tips, function(resultEle) {
            console.log(field)
            console.log(typeof(obj[field]))
            var jsondata = {
                port_id: obj[field].val
            }
            console.log(jsondata)
            $http({
                method: m,
                url: sendurl,
                data: jsondata, // pass in data as strings
            }).success(function(result) {
                console.log(result)
                if (m == 'post') { //Attach
                    // delete from available port_id_list
                    $scope.port_id_list = list_del($scope.port_id_list, obj[field])
                        // add to instances port_id_list
                    list_add(obj.port_list, obj[field])
                } else if (m == 'delete') { //Detach
                    // add to available port_id_list
                    list_add($scope.port_id_list, obj[field])
                        // delete from instances port_id_list
                    obj.port_list = list_del(obj.port_list, obj[field])
                }
                resultEle.html(tips + ' OK!')
            }).error(function(err) {
                console.log(err)
                resultEle.html(op + ' Failed:<pre>' + err + '</pre>')
            })
        })
    }
    $scope.setIPFormData = {}
    $scope.setIPFormSubmit = function(senddata, sendurl) {
        // console.log(senddata)
        delete senddata.error
        delete senddata.success
            // organize parameters in mydata
        var mydata = {}
        for (var prop in senddata) {
            if ((typeof(senddata[prop]) == "object") && ('key' in senddata[prop])) {
                mydata[prop] = senddata[prop].key
            } else {
                mydata[prop] = senddata[prop]
            }
        }
	delete mydata.show
        var tips = '申请ip地址: <pre>' + JSON.stringify(mydata, null, 4) + '</pre>'
        for (var prop in senddata) {
            if ((typeof(senddata[prop]) == "object") && ('val' in senddata[prop])) {
                mydata[prop] = senddata[prop].val
            }
        }

        console.log(senddata)
        console.log(mydata)
        confirmModal(tips, function(resultEle) {
            $http({
                method: 'POST',
                url: sendurl,
                data: mydata, // pass in data as strings
            }).success(function(result) {
                console.log(result)
                senddata.success = 'ip allocated OK: '
                for (var i in result.items) {
                    console.log(result.items[i])
                    list_add($scope.port_id_list, result.items[i])
                    senddata.success += result.items[i].key + ' '
                }
                resultEle.html(senddata.success)
            }).error(function(err) {
                console.log(err)
                senddata.error = err
                resultEle.html('Failed:<pre>' + err + '</pre>')
            })
        })
    }
    $scope.setKeyFormData = {}
    $scope.setKeyFormSubmit = function(senddata, sendurl) {
        // console.log(senddata)
        delete senddata.error
        delete senddata.success
        $http({
            method: 'POST',
            url: sendurl,
            data: senddata, // pass in data as strings
            responseType: "arraybuffer"
        }).success(function(result) {
            // console.log(result)
            senddata.success = 'key generated OK: ' + senddata.keypair_name
            $scope.keypair_name_list.push({
                key: senddata.keypair_name,
                val: senddata.keypair_name
            })
            var blob = new Blob([result], {
                type: "application/binary"
            });
            var objectUrl = URL.createObjectURL(blob);
            window.open(objectUrl);
        }).error(function(err) {
            senddata.error = String.fromCharCode.apply(null, new Uint8Array(err))
        })
    }
    $scope.launchFormData = {
	    count: 1
    }
    $scope.launchFormSubmit = function(senddata, sendurl) {
        delete senddata.error
        delete senddata.success
            // organize parameters in mydata
        var mydata = {}
        for (var prop in senddata) {
            console.log(prop + typeof(senddata[prop]))
	    if (senddata[prop] != null) {
            	if ((typeof(senddata[prop]) == "object") && ('key' in senddata[prop])) {
            	    mydata[prop] = senddata[prop].key
            	} else {
            	    mydata[prop] = senddata[prop]
            	}
	    }
        }
	delete mydata.show
        var tips = '创建虚拟机: <pre>' + JSON.stringify(mydata, null, 4) + '</pre>'
        for (var prop in senddata) {
            if ((senddata[prop] != null) && (typeof(senddata[prop]) == "object") && ('val' in senddata[prop])) {
                mydata[prop] = senddata[prop].val
            }
        }
        console.log(senddata)
        console.log(mydata)
        confirmModal(tips, function(resultEle) {
            $http({
                method: 'POST',
                url: sendurl,
                data: mydata, // pass in data as strings
            }).success(function(result) {
                console.log(result)
                senddata.success = 'Instance launched OK!'
                resultEle.html(senddata.success)
            }).error(function(err) {
                console.log(err)
                senddata.error = err
                resultEle.html('Failed:<pre>' + senddata.error + '</pre>')
            })
        })
    }
    $http.get("/api/availzones/").then(function(response) {
        $scope.availzones_list = response.data.items;
    });
    $http.get("/api/networks/").then(function(response) {
        $scope.networks_list = response.data.items;
    });
    $http.get("/api/keypairs/").then(function(response) {
        $scope.keypair_name_list = response.data.items;
    });
    $http.get("/api/flavors/").then(function(response) {
        for (var i in response.data.items) {
            tmp = response.data.items[i]
            tmp.info = JSON.stringify({
                'ram': tmp.ram,
                'vcpus': tmp.vcpus,
                'disk': tmp.disk
            })
        }
        $scope.flavor_list = response.data.items;
    });
    $http.get("/api/images/").then(function(response) {
        $scope.image_list = response.data.items;
    });
    $http.get("/api/users/").then(function(response) {
        $scope.owner_list = response.data.items;
    });
    $http.get("/api/ports/").then(function(response) {
        $scope.port_id_list = response.data.items;
    });
    $http.get("/api/servers/").then(function(response) {
        $scope.same_host_list = response.data.items;
    });
    $scope.changedValue = function(item) {
        var urlpre = '/api/hosts/'
        $http.get(urlpre + item.val + '/').then(function(response) {
            $scope.host_list = response.data.items;
        });
    }
});
/********************
 * volumes/index.html
 ********************/
var app = angular.module('volumesApp', []);
app.config(function($httpProvider, $interpolateProvider) {
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    // http://stackoverflow.com/questions/18156452/django-csrf-token-angularjs
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
});
app.controller('volumesCtrl', function($scope, $http) {
	//s
	$scope.attachment_list = function(data, uuid) {
	    $http.get("/api/volumes/" + uuid + "/").then(function(response) {
	        data.attachment_list = response.data.items
	    });
	}
	$scope.toggle = toggleFunc
	$scope.volumeAction = function(uuid, name, action, senddata) {
	    console.log(action)
	    console.log(senddata)
	    if (typeof(senddata) == "undefined") {
                return
            }
	    var urlpre = '/api/volumes/'
	    tips = '即将对 ' + name + ' 存储卷进行 ' + action
	    confirmModal(tips, function(resultEle) {
	        if (action == 'delete') {
	            m = 'delete'
	        } else {
	            m = 'post'
	        }
	        $http({
	            method: m,
	            url: urlpre + uuid + '/',
	            data: {
	                action: action,
			data: senddata
	            } // pass in data as strings
	        }).success(function(result) {
	            console.log(result)
	            resultEle.html(action + ' Success!')
	        }).error(function(err) {
	            console.log(err)
	            resultEle.html(action + ' Error: ' + err)
	        })
	    })
	}
	$http.get("/api/availzones/").then(function(response) {
		$scope.availzones_list = response.data.items;
    	});
	$http.get("/api/snapshots/").then(function(response) {
		$scope.snapshot_source_list = response.data.items;
    	});
        $http.get("/api/servers/").then(function(response) {
            $scope.local_to_instance_list = response.data.items;
        });
	$scope.volumeFormData = {}
	$scope.volumeFormSubmit = function(obj, field, sendurl, m) {
	        console.log(field)
	        console.log(typeof(obj[field]))
	        if (typeof(obj[field]) == "undefined") {
	            return
	        }
	        if (m == 'post') {
	            op = 'Attach'
	        } else if (m == 'delete') {
	            op = 'Detach'
	        }
	        var tips = op + ' 存储卷: ' + obj[field].key;
	        confirmModal(tips, function(resultEle) {
			var jsondata = {
		                instance_id: obj[field].val
        	        }
        	        console.log(jsondata)
        	        $http({
        	            method: m,
        	            url: sendurl,
        	            data: jsondata, // pass in data as strings
        	        }).success(function(result) {
        	            console.log(result)
			    /*
        	            if (m == 'post') { //Attach
        	                // delete from available port_id_list
        	                $scope.port_id_list = list_del($scope.port_id_list, obj[field])
        	                    // add to instances port_id_list
        	                list_add(obj.port_list, obj[field])
        	            } else if (m == 'delete') { //Detach
        	                // add to available port_id_list
        	                list_add($scope.port_id_list, obj[field])
        	                    // delete from instances port_id_list
        	                obj.port_list = list_del(obj.port_list, obj[field])
        	            }
			    */
        	            resultEle.html(tips + ' OK!')
        	        }).error(function(err) {
        	            console.log(err)
        	            resultEle.html(op + ' Failed:<pre>' + err + '</pre>')
        	        })
		})
	}


	$scope.createVolFormData = {
		size: 1
	}
    	$scope.createVolFormSubmit = function(senddata, sendurl) {
    	    // console.log(senddata)
    	    delete senddata.error
    	    delete senddata.success
    	    // organize parameters in mydata
    	    var mydata = {}
	    console.log(senddata)
    	    for (var prop in senddata) {
    	        if (senddata[prop] != null) {
			if ((typeof(senddata[prop]) == "object") && ('key' in senddata[prop])) {
    	            		mydata[prop] = senddata[prop].key
    	        	} else {
    	            		mydata[prop] = senddata[prop]
    	        	}
		}
	    }
	    delete mydata.show
    	    var tips = '创建存储卷: <pre>' + JSON.stringify(mydata, null, 4) + '</pre>'
    	    for (var prop in senddata) {
    	        if ((senddata[prop] != null) && 
			(typeof(senddata[prop]) == "object") && ('val' in senddata[prop])) {
    	            mydata[prop] = senddata[prop].val
    	        }
    	    }

    	    console.log(senddata)
    	    console.log(mydata)
    	    confirmModal(tips, function(resultEle) {
    	        $http({
    	            method: 'POST',
    	            url: sendurl,
    	            data: mydata, // pass in data as strings
    	        }).success(function(result) {
    	            console.log(result)
    	            senddata.success = 'volume created OK: '+mydata.name
    	            resultEle.html(senddata.success)
    	        }).error(function(err) {
    	            console.log(err)
    	            senddata.error = err
    	            resultEle.html('Failed:<pre>' + err + '</pre>')
    	        })
    	    })
    	}
})
