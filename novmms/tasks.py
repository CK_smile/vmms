# celery tasks
from __future__ import absolute_import

from celery import shared_task
from novmms import api
from novmms.dashboard import utils

@shared_task
def sync_instance_info():
    try:
        instances = []
        instances = api.nova.server_list(None, all_tenants = True,
                                         is_admin=True)
        utils.handle_instances(None, instances, is_admin=True)
    except Exception: # ignore all fails
        pass
