#coding:utf-8  
from django import template
from django.utils.safestring import SafeText
register = template.Library()

@register.filter
def is_in(var, obj):
    #return var in obj
    if isinstance(obj, SafeText):
        return var in obj.split(' ')
    else:
        return var in obj

@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)

@register.filter
def klass(ob):
    return ob.__class__.__name__
