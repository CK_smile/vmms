"""novmms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from novmms.auth.forms import Login

# patch admin
from django.contrib import admin
admin.site.login_form = Login
from django.contrib.admin import models as admin_models
def log_action(*args, **kwargs): # disable django_admin_log(Or can't change obj on the admin)
    pass
admin_models.LogEntryManager.log_action = log_action

# patch:
from openstack_auth import utils
utils.patch_middleware_get_user()

from django.utils import functional
from django.contrib.auth import decorators
# def my_permission_required():
#     return functional.curry(decorators.permission_required,
#                             login_url='/auth/logout', raise_exception=True)
decorators.permission_required = functional.curry(decorators.permission_required,
                                                  raise_exception=True)

import json
from datetime import datetime
def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""
    if isinstance(obj, datetime):
        serial = obj.isoformat()
        return serial
    raise TypeError("Type not serializable")
json.dumps = functional.curry(json.dumps, default=json_serial)

# urls:
urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^auth/', include("novmms.auth.urls", namespace="auth")),
    url(r'^overview/', include("novmms.dashboard.overview.urls",
                               namespace="overview")),
    url(r'^instances/', include("novmms.dashboard.instances.urls",
                                namespace="instances")),
    url(r'^volumes/', include("novmms.dashboard.volumes.urls",
                              namespace="volumes")),
    url(r'^api/', include('novmms.api.rest.urls', namespace="api")),
]
