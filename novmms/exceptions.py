#coding=utf-8

class AuthFailed(Exception):
    """Raised when a user is trying to make requests and they are not
    pass the ldap auth.
    """
    pass

class AjaxError(Exception):
    def __init__(self, http_status, msg):
        self.http_status = http_status
        super(AjaxError, self).__init__(msg)
