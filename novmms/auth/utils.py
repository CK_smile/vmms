#coding=utf-8
from novmms.api import keystone
from keystoneclient import exceptions as keystone_exceptions

def registerUserInKeystone(request, user_info):
    """
    若user未在keystone中注册，则进行用户创建工作
    """
    u_list = keystone.username_list(request)
    if user_info['username'] in u_list:
        return True
    else: # User not exist, create it
        try:
            keystone.user_create(request, name=user_info['username'],
                                 email=user_info['email'],
                                 password=user_info['password'],
                                 project=user_info['project'],
                                 role=user_info['role'])
        except Exception:
            raise
        return True
