#coding=utf-8
#这方法貌似不行 openstack_auth中使用的User没法修改到
from openstack_auth import user as auth_user

class User(auth_user.User):

    @property
    def is_staff(self):
        return self.is_superuser

    def get_short_name(self):
        return self.username
