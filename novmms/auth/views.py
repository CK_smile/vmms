from django.shortcuts import render,redirect
from django.conf import settings
from django.utils import functional
from django.contrib.auth import authenticate

from keystoneclient.v2_0 import client as keystoneclient
from openstack_auth import user as auth_user

from novmms.auth.forms import LoginForm

import datetime

from django.contrib import auth
from django.contrib.auth import views as django_auth_views
from django.views.decorators.cache import never_cache  # noqa
from django.views.decorators.csrf import csrf_protect  # noqa
from django.utils import timezone

from novmms.auth.forms import Login

import logging
logger = logging.getLogger(__name__)

@csrf_protect
@never_cache
def login(request, template_name=None, extra_context=None, **kwargs):
    if not request.is_ajax():
        if (request.user.is_authenticated() and
                auth.REDIRECT_FIELD_NAME not in request.GET and
                auth.REDIRECT_FIELD_NAME not in request.POST):
            return redirect(settings.LOGIN_REDIRECT_URL)

    initial = {}
    current_region = request.session.get('region_endpoint', None)
    requested_region = request.POST.get('region', request.GET.get('region',
                                                                   None))
    regions = dict(getattr(settings, "AVAILABLE_REGIONS", []))
    if requested_region in regions and requested_region != current_region:
        initial.update({'region': requested_region})
    if request.method == "POST":
        loginform = functional.curry(Login)
    else:
        loginform = functional.curry(Login, initial=initial)
    if extra_context is None:
        extra_context = {'redirect_field_name': auth.REDIRECT_FIELD_NAME}
    if not template_name:
        template_name = 'novmms/auth/login.html'
    res = django_auth_views.login(request,
                                  template_name=template_name,
                                  authentication_form=loginform,
                                  extra_context=extra_context,
                                  **kwargs)
    if request.method == "POST":
        set_response_cookie(res, 'login_region',
                                  request.POST.get('region', ''))
        set_response_cookie(res, 'login_domain',
                                  request.POST.get('domain', ''))
    if request.user.is_authenticated():
        logger.info('User %s Logged in' % (request.user.username,))
        auth_user.set_session_from_user(request, request.user)
        regions = dict(Login.get_region_choices())
        region = request.user.endpoint
        region_name = regions.get(region)
        request.session['region_endpoint'] = region
        request.session['region_name'] = region_name
#    request.COOKIES['logout_reason'] = request.environ.get('REMOTE_ADDR', '')
#    res.set_cookie('logout_reason', 'reason', max_age=10)
#    if 'logout_reason' in request.COOKIES:
#        res.delete_cookie('logout_reason')
    return res

def set_response_cookie(response, cookie_name, cookie_value):
    now = timezone.now()
    expire_date = now + datetime.timedelta(days=365)
    response.set_cookie(cookie_name, cookie_value, expires=expire_date)

def logout(request, login_url=None, **kwargs):
    endpoint = request.session.get('region_endpoint')
    # delete the project scoped token
    token = request.session.get('token')
    if token and endpoint:
        delete_token(endpoint=endpoint, token_id=token.id)
    """ Securely logs a user out. """
    logger.info('User %s Log out' % (request.user.username,))
    return django_auth_views.logout_then_login(request,
                                               login_url=login_url,
                                               **kwargs)

def delete_token(endpoint, token_id):
    try:
        from keystoneauth1 import session, token_endpoint
        session = session.Session(verify=False)
        auth_plugin = token_endpoint.Token(endpoint=endpoint,
                                           token=token_id)
        from keystoneclient.v2_0 import client as client_v2
        client = client_v2.Client(session=session,
                                  auth=auth_plugin)
        client.tokens.delete(token=token_id)
    except Exception: # keystone_exceptions.ClientException
        pass

def login2(request, template_name=None, extra_context=None, **kwargs):
    # Get our initial region for the form.
    initial = {}
    current_region = request.session.get('region_endpoint', None)
    requested_region = request.GET.get('region', None)
    regions = dict(getattr(settings, "AVAILABLE_REGIONS", []))
    if requested_region in regions and requested_region != current_region:
        initial.update({'region': requested_region})
    if request.method == "POST":
        loginform = functional.curry(LoginForm)
    else:
        loginform = functional.curry(LoginForm, initial=initial)
    # Decide template_name.
    if not template_name:
        template_name = 'auth/login.html'
    if request.method == "POST":
        form = loginform(request.POST)
        next_url = "/instances/" 

        if form.is_valid():
            data = form.cleaned_data
            user = data["user"]
            password = data["password"]

            user1 = authenticate(auth_url='http://10.11.157.184:5000/v2.0', 
                                   username=user, password=password, request=request)
            if not user1:
                return redirect(next_url)
            current_region_name = 'RegionOne'
            auth_url = 'http://10.11.157.184:35357/v2.0' 
            tenant_name = 'admin'
            user= 'admin'
            password = 'redhat'

            keystone = keystoneclient.Client(username=user,
                                  password=password,
                                  tenant_name=tenant_name,
                                  auth_url=auth_url)
            auth_ref = keystone.auth_ref
            token = auth_user.Token(auth_ref)
            user = auth_user.create_user_from_token(request, token,
                auth_ref.service_catalog.url_for(endpoint_type="public"))
            user.auth_ref = auth_ref
            # <User: admin> is not JSON serializable
            # http://stackoverflow.com/questions/24229397/django-object-is-not-json-serializable-error-after-upgrading-django-to-1-6-5
            request.session['user'] = user
            request.session['region'] = current_region_name
            #if "?next=" in full_url:
            #    return redirect(full_url.split("?next=")[-1])
            #else:
            #    return redirect(settings.LOGIN_URL)
            response = redirect(next_url)
            try:
                user.auth_ref.service_catalog.url_for(service_type = 'volumev2')
                request.session['has_cinder_service'] = True
                response.set_cookie("has_cinder_service","true")
            except Exception as e:
                request.session['has_cinder_service'] = False
                response.set_cookie("has_cinder_service","flase")
            return response 
        else:
            return render(request,'login.html', {'form': form})
    else:
        form = loginform()
        return render(request,template_name, {'form': form,})

def logout2(request):
    request.session.clear()
    return redirect("auth:login")

