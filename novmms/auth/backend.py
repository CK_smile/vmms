from openstack_auth import backend as auth_backend

import logging
logger = logging.getLogger(__name__)

class KeystoneBackend(auth_backend.KeystoneBackend):
    def has_module_perms(self, user, app_label):
        if 'novmms' in app_label and \
           user.has_perm('openstack.roles.admin'): # for django admin
            return True
        else:
            return super(KeystoneBackend, self).has_module_perms(user, app_label)
    def has_perm(self, user, perm, obj=None):
        logger.debug('Check User Permissions %s, %s' % (user.username, perm))
        if 'novmms' in perm and \
           super(KeystoneBackend, self).has_perm(user, 'openstack.roles.admin',
                                                obj=obj): # for django admin
            return True
        else:
            return super(KeystoneBackend, self).has_perm(user, perm, obj=obj)


