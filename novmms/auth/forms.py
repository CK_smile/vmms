#coding:utf-8
from django import forms
from novmms.thirdpart.sohu_ldap import LdapAuth

class LoginForm(forms.Form):
    user = forms.CharField(
        widget=forms.TextInput(),
        max_length=50,label="用户")
    password = forms.CharField(
        widget=forms.PasswordInput(),
        max_length=50,label="密码")

import collections
from django.contrib.auth import forms as django_auth_forms
from django.conf import settings
from django.contrib.auth import authenticate
from django.db.utils import IntegrityError
from openstack_auth import exceptions
from novmms.exceptions import AuthFailed
from novmms.auth.utils import registerUserInKeystone
from openstack_auth import user as auth_user

import logging
logger = logging.getLogger(__name__)

class Login(django_auth_forms.AuthenticationForm):
    region = forms.ChoiceField(label="Region", required=False)
    username = forms.CharField(
                label="UserName",
                widget=forms.TextInput(attrs={"autofocus": "autofocus"}),
                required=True)
    password = forms.CharField(
                label="Password",
                widget=forms.PasswordInput(render_value=False), required=True)
    def __init__(self, *args, **kwargs):
        super(Login, self).__init__(*args, **kwargs)
        fields_ordering = ['username', 'password', 'region']
        self.fields['region'].choices = self.get_region_choices()
        if len(self.fields['region'].choices) == 1:
            self.fields['region'].initial = self.fields['region'].choices[0][0]
#            self.fields['region'].widget = forms.widgets.HiddenInput()
        elif len(self.fields['region'].choices) > 1:
            # 此值在views.py中登陆成功后会设置
            self.fields['region'].initial = self.request.COOKIES.get('login_region')

        self.fields = collections.OrderedDict(
                        (key, self.fields[key]) for key in fields_ordering)

    @staticmethod
    def get_region_choices():
        default_region = (settings.OPENSTACK_KEYSTONE_URL, "Default Region")
        regions = getattr(settings, 'AVAILABLE_REGIONS', [])
        if not regions:
            regions = [default_region]
        return regions

    def clean(self):
        default_domain = getattr(settings,
                                 'OPENSTACK_KEYSTONE_DEFAULT_DOMAIN',
                                 'Default')
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        region = self.cleaned_data.get('region')
        domain = self.cleaned_data.get('domain', default_domain)

        if not (username and password):
            return self.cleaned_data
        logger.info('User %s Try to log in' % (username,))

        # Auth against Sohu Ldap Service
        if username != 'admin' and not username.startswith('test'):
            user_info = LdapAuth().Auth(username, password)
            if not user_info:
                logger.warning('Sohu Ldap Auth Failed For User:%s' % (username,))
                raise forms.ValidationError(AuthFailed('Sohu ldap auth failed'))
            else:
                try:
                    registerUserInKeystone(self.request, user_info)
                except Exception as exc:
                    raise forms.ValidationError(exc)
        # Auth against KeyStone
        try:
            self.user_cache = authenticate(request=self.request,
                                           username=username,
                                           password=password,
                                           user_domain_name=domain,
                                           auth_url=region)
        except exceptions.KeystoneAuthException as exc:
            logger.warning('Keystone Auth Failed For User:%s' % (username,))
            raise forms.ValidationError(exc)
        # set session here, so that admin view can work
        if self.request.user.is_authenticated() and \
        self.request.user.has_perm('openstack.roles.admin'): # set session for admin here, for django admin use
            logger.debug('role:admin Logged in, setsession in advince')
            auth_user.set_session_from_user(self.request, self.request.user)
           # try:
           #     auth_user.User(id=self.request.user.id,
           #                    password='kidding').save()
           # except IntegrityError:
           #     pass
        return self.cleaned_data
