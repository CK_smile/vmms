from django.contrib import admin

# Register your models here.
from novmms.dashboard.instances.models import Instance

class InstanceAdmin(admin.ModelAdmin):
    list_display = ('name', 'owner', 'project_name')
    list_filter = ['project_name', 'availability_zone', 'power_state', 'host']
    search_fields = ['name']
    fieldsets = [
        ('Basic Information', {'fields': ['owner', 'name', 'project_name', 'uuid',
                                          'availability_zone', 'status',
                                          'power_state']}),
        ('Extra Information', {'fields': ['host',
                                          'addresses', 'key_name',
                                          'launched_at', 'flavor', 'image',
                                          'project'],
                               'classes': ['collapse']}),
    ]
admin.site.register(Instance, InstanceAdmin)
